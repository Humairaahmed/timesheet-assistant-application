package com.mycompany.myapp.web.rest;

public class TimeEntry {
    public String description;
    public String projectId;
    public String start;
    public String end;
    public String billableStatus;
}
