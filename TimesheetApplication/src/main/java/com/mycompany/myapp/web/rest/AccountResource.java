package com.mycompany.myapp.web.rest;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.mycompany.myapp.security.SecurityUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;
import java.util.stream.Collectors;

//imports to make Api Calls
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import java.net.URL;
import java.net.MalformedURLException;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class AccountResource {

    public static String s_userID;
    public static String s_workSpaceID;


    public static String ApiKey;
    public static String[][] a_projects = new String[30][30];
    public static int i_numOfProjects = 0;

    //Time entry varaibles
   // public static String s_startTime = "\"2018-06-12T13:48:14.000Z\"";
   // public static String s_endTime = "\"2018-06-12T13:50:14.000Z\"";

    private static class AccountResourceException extends RuntimeException {
    }

    /**
     * {@code GET  /account} : get the current user.
     *
     * @return the current user.
     * @throws AccountResourceException {@code 500 (Internal Server Error)} if the user couldn't be returned.
     */
    @GetMapping("/account")
    public UserVM getAccount() {
        String login = SecurityUtils.getCurrentUserLogin()
            .orElseThrow(AccountResourceException::new);
        Set<String> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities().stream()
            .map(GrantedAuthority::getAuthority)
            .collect(Collectors.toSet());
        return new UserVM(login, authorities , null);
    }


    /*
        get request to save all project ID's and project names into an array
     */
    @GetMapping("getProjects")
    public UserVM getProjects(){
        char ch1;

        // Create an HTTP client so that we can hit the API:
        // https://openjdk.java.net/groups/net/httpclient/intro.html
        HttpClient client = HttpClient.newHttpClient();

        //Creating a url that is dynamic to the user
        String str = "https://api.clockify.me/api/v1/workspaces/" + s_workSpaceID + "/projects";
        String urlLink = "";
        try{
            URL link = new URL(str);
            urlLink = link.toString();
        }
        catch(MalformedURLException e) {
            e.printStackTrace();
        }

        // Create a new request:
        // curl -H "content-type: application/json" -H "X-Api-Key: yourAPIkey" -X GET https://api.clockify.me/api/v1/user
        HttpRequest request = HttpRequest.newBuilder()
            .uri(URI.create(urlLink))
            .header("Content-Type", "application/json")
            .header("X-Api-Key", ApiKey)
            .GET()
            .build();

        // Send the request:
        try{
            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());

            String[] parts = response.body().split("\\,");
            i_numOfProjects = 0;


            for (int k = 0 ; k < parts.length ; k += 28)
            {
                String[] item = parts[k].split("\\:");
                a_projects[i_numOfProjects][0] = item[1].substring( 1, item[1].length()-1);
                item = parts[k+1].split("\\:");
                a_projects[i_numOfProjects][1] = item[1].substring( 1, item[1].length()-1);
                ++i_numOfProjects;
            }


            return new UserVM (null,null,a_projects);
        }
        catch(Exception e)
        {
            return new UserVM("error has occured when getting  projects",null,null);
        }
    }

    /*
        Posting a new time entry into the users clokify account
     */
    @PostMapping("/PostToClokify")
    public UserVM postToClokifyAccount(@RequestBody TimeEntry timeentry){

        String s_description = "\""+ timeentry.description + "\"";
        String s_projectId = "\""+ timeentry.projectId + "\"";

        String s_startTime = "\"" + timeentry.start + "\"";
        String s_endTime = "\"" + timeentry.end + "\"";

        String s_billable_status = "\"" + timeentry.billableStatus + "\"";
        // Create an HTTP client so that we can hit the API:
        // https://openjdk.java.net/groups/net/httpclient/intro.html
        HttpClient client = HttpClient.newHttpClient();

        //Creating a url that is dynamic to the user
        String str = "https://api.clockify.me/api/v1/workspaces/" + s_workSpaceID + "/time-entries";
        String urlLink = "";
        try{
            URL link = new URL(str);
            urlLink = link.toString();
        }
        catch(MalformedURLException e) {
            e.printStackTrace();
        }

        //set json body for the post API call
        //https://mkyong.com/java/java-11-httpclient-examples/
        String timeEntry = new StringBuilder()
            .append("{")
            .append("\"start\":" + s_startTime + ",")
            .append("\"description\":" + s_description + ",")
            .append("\"projectId\":" + s_projectId + ",")
            .append("\"billable\":" + s_billable_status + ",")
            .append("\"end\":" + s_endTime +"")
            .append("}").toString();

        // Create a new request:
        // curl -H "content-type: application/json" -H "X-Api-Key: yourAPIkey" -X GET https://api.clockify.me/api/v1/user
        HttpRequest request = HttpRequest.newBuilder()
            .uri(URI.create(urlLink))
            .header("Content-Type", "application/json")
            .header("X-Api-Key", ApiKey)
            .POST(HttpRequest.BodyPublishers.ofString(timeEntry))
            .build();

        try
        {
            //Send request
            HttpResponse<String> response =
                client.send(request, HttpResponse.BodyHandlers.ofString());
            return new UserVM("posted", null,null);


        }
        catch (Exception e)
        {
            return new UserVM("error when posting",null,null);
        }
    }


    /*
        Method to get the user workspace Id
        Set to a variable called: s_workspaceID
     */
    @GetMapping("/getUserWorkspace")
    public UserVM getWorkspaceID(){
        char ch1;
        // Create an HTTP client so that we can hit the API:
        // https://openjdk.java.net/groups/net/httpclient/intro.html
        HttpClient client = HttpClient.newHttpClient();

        // Create a new request:
        // curl -H "content-type: application/json" -H "X-Api-Key: yourAPIkey" -X GET https://api.clockify.me/api/v1/user
        HttpRequest request = HttpRequest.newBuilder()
            .uri(URI.create("https://api.clockify.me/api/v1/workspaces"))
            .header("Content-Type", "application/json")
            .header("X-Api-Key", ApiKey)
            .GET()
            .build();

        // Send the request:
        try{
            HttpResponse<String> response =
                client.send(request, HttpResponse.BodyHandlers.ofString());
            //System.out.print("Output from Clokify to get workspaces\n");
            String[] split1 = (response.body()).split("\\,");
            String[] split2 = split1[0].split("\\,");
            String[] workspace = split2[0].split("\\:");

            //WorkspaceID
            s_workSpaceID = workspace[1];
            //removing the first and last characters, which are the inverted commas
            s_workSpaceID = s_workSpaceID.substring( 1, s_workSpaceID.length() - 1 );
            return new UserVM(s_workSpaceID,null,null);

        }
        catch(Exception e)
        {
            return new UserVM("error has occured when getting workspace ID",null,null);
        }

    }


    /*
        Method to get user information from Clokify
        This method also recives the Users Api key and sets it to the global variable to be used in other methods
        Used to get the users ID and save it in the global variable
     */
    @PostMapping("/GetUserInformation")
    public UserVM getUserID(@RequestBody UserInfo userInfo){

        //Setting the Api key
        ApiKey = userInfo.apiKey;


        // Create an HTTP client so that we can hit the API:
        // https://openjdk.java.net/groups/net/httpclient/intro.html
        HttpClient client = HttpClient.newHttpClient();

        // Create a new request:
        // curl -H "content-type: application/json" -H "X-Api-Key: yourAPIkey" -X GET https://api.clockify.me/api/v1/user
        HttpRequest request = HttpRequest.newBuilder()
            .uri(URI.create("https://api.clockify.me/api/v1/user"))
            .header("Content-Type", "application/json")
            .header("X-Api-Key", ApiKey)
            .GET()
            .build();

        // Send the request:
        try{
            HttpResponse<String> response =
                client.send(request, HttpResponse.BodyHandlers.ofString());

            //Get the userID and the workspaceID
            //This will be used for other Api calls

            //Spliting the data collected into an array, splitting at a comma
            String[] parts = response.body().split("\\,");

            //Splitting the data in the parts array at position 9, which is equal to the user ID into 2 parts splitting at :
            String[] user = parts[9].split("\\:");


            //setting the global variable
            //userID
            s_userID = user[1];
            //removing the first and last characters, which are the inverted commas
            s_userID = s_userID.substring( 1, s_userID.length() - 1 );


            //Display the userID
            return new UserVM(s_userID,null , null);


        }
        catch(Exception e)
        {
            return new UserVM("error in getting user ID",null ,null);
        }
    }




    private static class UserVM {
        private String login;
        private Set<String> authorities;
        private String arr[][];

        @JsonCreator
        UserVM(String login, Set<String> authorities , String arr[][]) {
            this.login = login;
            this.authorities = authorities;
            this.arr = arr;
        }

        public boolean isActivated() {
            return true;
        }

        public Set<String> getAuthorities() {
            return authorities;
        }

        public String getLogin() {
            return login;
        }

        public String[][] getArray(){
            return  arr;
        }
    }

    private static class connectToClokify{

        private String message;
        connectToClokify(String mes){
            this.message = mes;
        }
    }

}
