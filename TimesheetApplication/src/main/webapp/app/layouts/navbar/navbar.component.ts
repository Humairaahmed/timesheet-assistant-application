import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { VERSION } from 'app/app.constants';
import { AccountService } from 'app/core/auth/account.service';
import { LoginModalService } from 'app/core/login/login-modal.service';
import { LoginService } from 'app/core/login/login.service';
import { ProfileService } from 'app/layouts/profiles/profile.service';

@Component({
  selector: 'jhi-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['navbar.scss'],
})
export class NavbarComponent implements OnInit {
  inProduction?: boolean;
  isNavbarCollapsed = true;
  swaggerEnabled?: boolean;
  version: string;

  AccountOptions = "Sign In";

  constructor(
    private loginService: LoginService,
    private accountService: AccountService,
    private loginModalService: LoginModalService,
    private profileService: ProfileService,
    private router: Router
  ) {
    this.version = VERSION ? (VERSION.toLowerCase().startsWith('v') ? VERSION : 'v' + VERSION) : '';
  }

  DisplayTimeEntryOptions():void{
      const menu = document.getElementById("Display")!;
      const menu2 = document.getElementById("Display1")!;
      if (menu.style.display === "none"){
        menu.style.display = "block";
      }
      else{
        menu.style.display = "none";
      }
      if (menu2.style.display === "none"){
        menu2.style.display = "block";
      }
      else{
        menu2.style.display = "none";
      }
  }


  ngOnInit(): void {

    this.profileService.getProfileInfo().subscribe(profileInfo => {
      this.inProduction = profileInfo.inProduction;
      this.swaggerEnabled = profileInfo.swaggerEnabled;
    });
  }

  collapseNavbar(): void {
    this.isNavbarCollapsed = true;
  }

  isAuthenticated(): boolean {
    return this.accountService.isAuthenticated();
  }

  login(): void {
    this.loginModalService.open();
  }

  logout(): void {
    this.collapseNavbar();
    this.loginService.logout();
    this.router.navigate(['']);
  }

  toggleNavbar(): void {
    if (localStorage.getItem("SignInLabel") !== null)
    {
      this.AccountOptions = localStorage.getItem("SignInLabel")!;
    }
    (document.getElementById("Account") as HTMLInputElement)!.value= this.AccountOptions;
    this.isNavbarCollapsed = !this.isNavbarCollapsed;
    document.getElementById("navbarResponsive")!.style.borderRight = "3px solid #ECD812";
  }

  getImageUrl(): string {
    return this.isAuthenticated() ? this.accountService.getImageUrl() : '';
  }

}
