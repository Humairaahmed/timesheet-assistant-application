import { Routes } from '@angular/router';

import { ErrorComponent } from './error.component';

export const errorRoute: Routes = [
  {
    path: 'entry',
    component: ErrorComponent,
    data: {
      authorities: [],
      pageTitle: 'Manual Time Entry',
    },
  },
  {
    path: 'accessdenied',
    component: ErrorComponent,
    data: {
      authorities: [],
      pageTitle: 'Manual Time Entry',
      errorMessage: 'You are not authorized to access this page.',
    },
  },
  {
    path: '404',
    component: ErrorComponent,
    data: {
      authorities: [],
      pageTitle: 'Manual Time Entry',
      errorMessage: 'The page does not exist.',
    },
  },
  {
    path: '**',
    redirectTo: 'Manual Time Entry',
  },
];
