import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { AccountService } from 'app/core/auth/account.service';

// This service provides convenient methods for generating controls.
import { FormBuilder } from '@angular/forms';

import { LoginModalService } from 'app/core/login/login-modal.service';


@Component({
  selector: 'jhi-error',
  templateUrl: './error.component.html',
  styleUrls: ['timeEntry.scss'],
})
export class ErrorComponent implements OnInit {
  today = Date.now();
  errorMessage?: string;
  result = "";
  activities = [];
  project = -1 ;
  projectID = "";
  startTime = "";
  endTime = "";
  billable = "false";
  k = 0;

  TextBoxPlaceHolder = "What are you working on?";

    checkoutForm = this.formBuilder.group({
          name: ''
        });

  timeEntry : TimeEntry = null;

  errorResult = "";


  constructor(private route: ActivatedRoute, private accountService: AccountService,private formBuilder: FormBuilder, private loginModalService: LoginModalService) {this.timeEntry = {}}

  // when the button clicked to add the time entry to Clockify
  onSubmit(): void
  {
     if ((document.getElementById("DatePicked") as HTMLInputElement).value === "")
     {
        this.errorResult = "Date cannot be left blank";
        console.log(this.errorResult);
        const x = document.getElementById("ErrorMessage")!;
        x.style.display = "block";
     }
     else
     {
       this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex;
       console.log(this.project);
       if (this.project === 0)
       {
          this.errorResult = "Project cannot be left blank";
          console.log(this.errorResult);
          const x = document.getElementById("ErrorMessage")!;
          x.style.display = "block";
       }
       else
       {
         if ((document.getElementById("StartTimePicked") as HTMLInputElement).value > (document.getElementById("EndTimePicked") as HTMLInputElement).value)
         {
            // this.errorResult = "Pick an end time which is greater then the start time";
            console.log(this.errorResult);
            const x = document.getElementById("ErrorMessage")!;
             x.style.display = "block";
         }
         else
         {
            /* This will display on clockify ad 2 hours later
                when your Clockify time zone is set to : (UTC +2:00) Africa/Johannesburg
                Change you Clockify time Zone to: (UTC +00:00) GMT
                This will display the time entered */

            // Setting the start time and the end time to the right format
            this.startTime = (document.getElementById("DatePicked") as HTMLInputElement).value + "T" + (document.getElementById("StartTimePicked") as HTMLInputElement).value + ":00Z";
            this.endTime = (document.getElementById("DatePicked") as HTMLInputElement).value + "T" + (document.getElementById("EndTimePicked") as HTMLInputElement).value + ":00Z";

            // Entering and saving the data in TimeEntry.ts
           this.timeEntry =  { description: this.checkoutForm.value.name , projectId : this.projectID = this.activities[this.project-1][0] , start : this.startTime  , end : this.endTime , billableStatus : this.billable};

            // Calling the method in account.service.ts
            this.accountService.posting(this.timeEntry).subscribe();

           // Clearing  the input
           this.checkoutForm.reset();
           document.getElementById("billable")!.style.color = "#000000";
           this.billable = "false";
            const x = document.getElementById("Message")!;
           x.style.display = "block";
         }
       }
     }

  }


  // Checking if the project if billable or not
  isBillable(): void
  {
    if (this.billable === "true")
    {
      document.getElementById("billable")!.style.color = "#000000";
      this.billable = "false";
    }
    else
    {
      document.getElementById("billable")!.style.color = "#3FA5F7";
      this.billable = "true";
    }
    this.accountService.getProjects().subscribe(activities => this.activities = activities.array);
  }

  OkClicked():void{
    const x = document.getElementById("Message")!;
    x.style.display = "none";

  }

  OkClickedOnErrorMessage():void
  {
    const x = document.getElementById("ErrorMessage")!;
    x.style.display = "none";
  }

  // Getting the selected index to find the projectID in the array
  onProjectClick():void
  {
    this.accountService.getProjects().subscribe(activities => this.activities = activities.array);
    // adding to the project drop down menu
    const select = document.getElementById("ProjectsList")!;

    // Checking how many projects will be added to the drop down
    if (this.k === 0){
      while (this.activities[this.k][0] != null)
      {
          ++this.k;
      }
      // Adding to the drop down
      for (let i = 0; i < this.k; i++)
      {
        const optn = this.activities[i][1];
        const el = document.createElement("option");
        el.textContent = optn;
        el.value = optn;
        select.appendChild(el);
      }
    }
    this.project = (document.getElementById("ProjectsList")as HTMLSelectElement)!.selectedIndex;
  }

  AddProject():void{
    console.log("clicked add project");

    this.accountService.getProjects().subscribe();
  }

  AddToTasks():void{
  //  this.userInfo =  { apiKey: "this.checkoutForm.value.name" }
    console.log("Clicked add to clokify");
   // this.accountService.posting(this.userInfo).subscribe(data => {true});
  }

  ngOnInit(): void {
    this.route.data.subscribe(routeData => {
      if (routeData.errorMessage) {
        this.errorMessage = routeData.errorMessage;
      }
    });
  //  this.accountService.getProjects().subscribe(activities => this.activities = activities.array);
    this.accountService.getProjects().subscribe(activities => this.activities = activities.array);
  }
}
