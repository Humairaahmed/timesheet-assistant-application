import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { flatMap } from 'rxjs/operators';

import { MetricsService, Metrics, MetricsKey, ThreadDump, Thread } from './metrics.service';

import { AccountService } from 'app/core/auth/account.service';


@Component({
  selector: 'jhi-metrics',
  templateUrl: './metrics.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['scheduler.scss'],
})
export class MetricsComponent implements OnInit {
  metrics?: Metrics;
  threads?: Thread[];
  updatingMetrics = true;
  today = Date.now();

  activities = [];
    k = 0;

  backgroundColour = [
                        ["#C1E6F9"],
                        ["#8FC6E1"],
                        ["#73CCFB"],
                        ["#56B4E4"],
                        ["#51BEF6"],
                        ["#37BAFE"]
                     ];

  startTime = "";

  tasks = [
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""],
              ["", "" , "" , "", ""]
          ];

  numTasks = 0;

  seven = "";
  sevenProject = "";
  sevenBillable = "";

  sevenFifteen = "";
  sevenFifteenProject = "";
  sevenFifteenBillable = "";

  sevenThirty = "";
  sevenThirtyProject = "";
  sevenThirtyBillable = "";

  sevenFortyFive = "";
  sevenFortyFiveProject = "";
  sevenFortyFiveBillable = "";

  eight = "";
  eightProject = "";
  eightBillable = "";

  eightFifteen = "";
  eightFifteenProject = "";
  eightFifteenBillable = "";

  eightThirty = "";
  eightThirtyProject = "";
  eightThirtyBillable = "";

  eightFortyFive = "";
  eightFortyFiveProject = "";
  eightFortyFiveBillable = "";

  nine = "";
  nineProject = "";
  nineBillable = "";

  nineFifteen = "";
  nineFifteenProject = "";
  nineFifteenBillable = "";

  nineThirty = "";
  nineThirtyProject = "";
  nineThirtyBillable = "";

  nineFortyFive = "";
  nineFortyFiveProject = "";
  nineFortyFiveBillable = "";

  ten = "";
  tenProject = "";
  tenBillable = "";

  tenFifteen = "";
  tenFifteenProject = "";
  tenFifteenBillable = "";

  tenThirty = "";
  tenThirtyProject = "";
  tenThirtyBillable = "";

  tenFortyFive = "";
  tenFortyFiveProject = "";
  tenFortyFiveBillable = "";

  eleven = "";
  elevenProject = "";
  elevenBillable = "";

  elevenFifteen = "";
  elevenFifteenProject = "";
  elevenFifteenBillable = "";

  elevenThirty = "";
  elevenThirtyProject = "";
  elevenThirtyBillable = "";

  elevenFortyFive = "";
  elevenFortyFiveProject = "";
  elevenFortyFiveBillable = "";

  twelve = "";
  twelveProject = "";
  twelveBillable = "";

  twelveFifteen = "";
  twelveFifteenProject = "";
  twelveFifteenBillable = "";

  twelveThirty = "";
  twelveThirtyProject = "";
  twelveThirtyBillable = "";

  twelveFortyFive = "";
  twelveFortyFiveProject = "";
  twelveFortyFiveBillable = "";

  thirteen = "";
  thirteenProject = "";
  thirteenBillable = "";

  thirteenFifteen = "";
  thirteenFifteenProject = "";
  thirteenFifteenBillable = "";

  thirteenThirty = "";
  thirteenThirtyProject = "";
  thirteenThirtyBillable = "";

  thirteenFortyFive = "";
  thirteenFortyFiveProject = "";
  thirteenFortyFiveBillable = "";

  fourteen = "";
  fourteenProject = "";
  fourteenBillable = "";

  fourteenFifteen = "";
  fourteenFifteenProject = "";
  fourteenFifteenBillable = "";

  fourteenThirty = "";
  fourteenThirtyProject = "";
  fourteenThirtyBillable = "";

  fourteenFortyFive = "";
  fourteenFortyFiveProject = "";
  fourteenFortyFiveBillable = "";

  fifteen = "";
  fifteenProject = "";
  fifteenBillable = "";

  fifteenFifteen = "";
  fifteenFifteenProject = "";
  fifteenFifteenBillable = "";

  fifteenThirty = "";
  fifteenThirtyProject = "";
  fifteenThirtyBillable = "";

  fifteenFortyFive = "";
  fifteenFortyFiveProject = "";
  fifteenFortyFiveBillable = "";

  sixteen = "";
  sixteenProject = "";
  sixteenBillable = "";

  sixteenFifteen = "";
  sixteenFifteenProject = "";
  sixteenFifteenBillable = "";

  sixteenThirty = "";
  sixteenThirtyProject = "";
  sixteenThirtyBillable = "";

  sixteenForthFive = "";
  sixteenForthFiveProject = "";
  sixteenForthFiveBillable = "";

  seventeen = "";
  seventeenProject = "";
  seventeenBillable = "";

  seventeenFifteen = "";
  seventeenFifteenProject = "";
  seventeenFifteenBillable = "";


  project  = 0;
  projectID = "";

  item = "";

  datePicked = "";

  billable = "false";


  timeEntry : TimeEntry = null;

  ErrorResult = "";

  originalDate = "";

  constructor(private metricsService: MetricsService, private changeDetector: ChangeDetectorRef , private accountService: AccountService) {}

   /* This will display on clockify ad 2 hours later
      when your Clockify time zone is set to : (UTC +2:00) Africa/Johannesburg
      Change you Clockify time Zone to: (UTC +00:00) GMT
      This will display the time entered */

  // Date picker when the page is loaded
  DatePicked():void{
    const x = document.getElementById("DatesPicked")!;
    if ((document.getElementById("dateChosen") as HTMLInputElement).value !== "")
    {
      this.accountService.getProjects().subscribe(activities => this.activities = activities.array);
      this.datePicked = (document.getElementById("dateChosen") as HTMLInputElement).value ;
      this.originalDate = this.datePicked;
      x.style.display = "none";
      const error = document.getElementById("errorMessage")!;
      error.style.display = "none";
      // enabling all buttons
      const disableTime = (document.getElementById("DatePicked") as HTMLInputElement)!;
      disableTime.disabled = false;
      const disabledNextTask = (document.getElementById("btnNext") as HTMLInputElement)!;
      disabledNextTask.disabled = false;
      const disabledTask = (document.getElementById("tasks") as HTMLInputElement)!;
      disabledTask.disabled = false;
      const disabledProjects = (document.getElementById("ProjectsList") as HTMLInputElement)!;
      disabledProjects.disabled = false;
      const disabledAddButton = (document.getElementById("btnAdd") as HTMLInputElement)!;
      disabledAddButton.disabled = false;
      const disabledRemoveButton = (document.getElementById("remove") as HTMLInputElement)!;
      disabledRemoveButton.disabled = false;

      this.accountService.getProjects().subscribe(activities => this.activities = activities.array);
    }
    else
    {
      const error = document.getElementById("errorMessage")!;
      error.style.display = "block";

    }

  }

  // Change the date to the next day
  Next():void
  {
  const dattmp = this.datePicked;
  const nwdate =  new Date(dattmp);

  nwdate.setDate(nwdate.getDate()+1);


    if (nwdate.getDay() < 10)
          {
            if ((nwdate.getMonth()+1)  < 10)
            {
              // Setting the date of the next day
              const next =  new Date(nwdate.getFullYear() + "-0" + (nwdate.getMonth()+1) + "-" + "0" + nwdate.getDay());

              // Converting to the correct format yyy-MM-dd
              const nextDay = new Date(next),
              month = ("0" + (nextDay.getMonth() + 1)).slice(-2),
              day = ("0" + nextDay.getDate()).slice(-2);

              // Adding it to the calender
              this.datePicked = [nextDay.getFullYear(), month, day].join("-")  ;
            }
            else
            {
              // Setting the date of the next day
              const next =  new Date(nwdate.getFullYear() + "-" + (nwdate.getMonth()+1) + "-" + "0" + nwdate.getDay());
              // Converting to the correct format yyy-MM-dd
              const nextDay = new Date(next),
              month = ((nextDay.getMonth() + 1)).slice(-2),
              day = ("0" + nextDay.getDate()).slice(-2);

              // Adding it to the calender
              this.datePicked = [nextDay.getFullYear(), month, day].join("-")  ;
            }
          }
          else
          {
            if ((nwdate.getMonth()+1)  < 10)
            {
              // Setting the date of the next day
              const next =  new Date(nwdate.getFullYear() + "-0" + (nwdate.getMonth()+1) + "-"  + nwdate.getDay());

              // Converting to the correct format yyy-MM-dd
              const nextDay = new Date(next),
              month = ("0" + (nextDay.getMonth() + 1)).slice(-2),
              day = ( nextDay.getDate()).slice(-2);

              // Adding it to the calender
              this.datePicked = [nextDay.getFullYear(), month, day].join("-")  ;
            }
            else
            {
              // Setting the date of the next day
              const next =  new Date(nwdate.getFullYear() + "-" + (nwdate.getMonth()+1) + "-" + nwdate.getDay());
              // Converting to the correct format yyy-MM-dd
              const nextDay = new Date(next),
              month = ( (nextDay.getMonth() + 1)).slice(-2),
              day = ( nextDay.getDate()).slice(-2);

              // Adding it to the calender
              this.datePicked = [nextDay.getFullYear(), month, day].join("-")  ;
            }

          }
          document.getElementById("PreviousDay")!.style.display = "block";
          document.getElementById("NextDay")!.style.display = "none";
          if (this.originalDate === this.datePicked)
          {
            document.getElementById("PreviousDay")!.style.display = "block";
            document.getElementById("NextDay")!.style.display = "block";
          }
  }

  // Change the date to the previous day
  PreviousDay():void
    {
    const dattmp = this.datePicked;
    const nwdate =  new Date(dattmp);

    nwdate.setDate(nwdate.getDate()-1);
    console.log(nwdate);


      if (nwdate.getDay() < 10)
      {
        if ((nwdate.getMonth()+1)  < 10)
        {
          // Setting the date of the next day
          const next =  new Date(nwdate.getFullYear() + "-0" + (nwdate.getMonth()+1) + "-" + "0" + nwdate.getDay());

          // Converting to the correct format yyy-MM-dd
          const nextDay = new Date(next),
          month = ("0" + (nextDay.getMonth() + 1)).slice(-2),
          day = ("0" + nextDay.getDate()).slice(-2);

          // Adding it to the calender
          this.datePicked = [nextDay.getFullYear(), month, day].join("-")  ;
        }
        else
        {
          // Setting the date of the next day
          const next =  new Date(nwdate.getFullYear() + "-" + (nwdate.getMonth()+1) + "-" + "0" + nwdate.getDay());
          // Converting to the correct format yyy-MM-dd
          const nextDay = new Date(next),
          month = ((nextDay.getMonth() + 1)).slice(-2),
          day = ("0" + nextDay.getDate()).slice(-2);

          // Adding it to the calender
          this.datePicked = [nextDay.getFullYear(), month, day].join("-")  ;
        }
      }
      else
      {
        if ((nwdate.getMonth()+1)  < 10)
        {
          // Setting the date of the next day
          const next =  new Date(nwdate.getFullYear() + "-0" + (nwdate.getMonth()+1) + "-"  + nwdate.getDay());

          // Converting to the correct format yyy-MM-dd
          const nextDay = new Date(next),
          month = ("0" + (nextDay.getMonth() + 1)).slice(-2),
          day = ( nextDay.getDate()).slice(-2);

          // Adding it to the calender
          this.datePicked = [nextDay.getFullYear(), month, day].join("-")  ;
        }
        else
        {
          // Setting the date of the next day
          const next =  new Date(nwdate.getFullYear() + "-" + (nwdate.getMonth()+1) + "-" + nwdate.getDay());
          // Converting to the correct format yyy-MM-dd
          const nextDay = new Date(next),
          month = ( (nextDay.getMonth() + 1)).slice(-2),
          day = ( nextDay.getDate()).slice(-2);

          // Adding it to the calender
          this.datePicked = [nextDay.getFullYear(), month, day].join("-")  ;
        }

      }
      document.getElementById("PreviousDay")!.style.display = "none";
      document.getElementById("NextDay")!.style.display = "block";
      if (this.originalDate === this.datePicked)
      {
        document.getElementById("PreviousDay")!.style.display = "block";
        document.getElementById("NextDay")!.style.display = "block";
      }

    }

  // Closing the successful message
  OkClicked():void{
      const x = document.getElementById("Message")!;
      x.style.display = "none";
    }
  // Display the error message when no date is picked
  OkClickedOnError():void{
      const x = document.getElementById("ErrorResultMessage")!;
      x.style.display = "none";
    }

  // Display the message to delete entries or cancel
  RemoveEntries():void
  {
    const x = document.getElementById("MessageRemove")!;
    x.style.display = "block";
  }

  // Cancelling
  cancel():void{
    const x = document.getElementById("MessageRemove")!;
    x.style.display = "none";
  }

  // Removing all entries
  remove():void{
    for (let k =0 ; k <= this.numTasks ; ++k)
    {
        for (let i =0 ; i <= 3 ; ++i)
        {
          this.tasks[k][i] = "";
        }
    }
    this.numTasks = 0;
    const x = document.getElementById("MessageRemove")!;
    x.style.display = "none";
  }


  // Checking if the project if billable or not
    isBillable(): void
    {
      if (this.billable === "true")
      {
        document.getElementById("billable")!.style.color = "#000000";
        this.billable = "false";
      }
      else
      {
        document.getElementById("billable")!.style.color = "#3FA5F7";
        this.billable = "true";
      }
    }

  // setting the task for 7:00
  Clicked7():void{
    // Checking if the project is blank
    if ((document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1 === -1)
    {
      this.ErrorResult = "Project cannot be left blank";
      const x = document.getElementById("ErrorResultMessage")!;
      x.style.display = "block";
    }
    else
    {
      // Duplicating the task setting
      if ((document.getElementById("tasks") as HTMLInputElement).value === "")
      {
        (document.getElementById("tasks") as HTMLInputElement).value = this.seven;
        for (let i = 0; i < this.k ; ++i)
        {
          if (this.activities[i][0] === this.sevenProject)
          {
            (document.getElementById("ProjectsList") as HTMLInputElement).value = this.activities[i][1];
          }

        }
        // Setting the billable status
        this.billable = this.sevenBillable;
        if (this.billable === "false")
        {
          document.getElementById("billable")!.style.color = "#000000";
        }
        else
        {
          document.getElementById("billable")!.style.color = "#3FA5F7";
        }
      }
      else
      {
        this.seven = (document.getElementById("tasks") as HTMLInputElement).value;
        if (this.seven === this.sevenFifteen)
        {
          document.getElementById("7")!.style.background = document.getElementById("715")!.style.backgroundColor;
          this.tasks[this.numTasks][1] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T07:00:00Z";
          this.sevenProject = this.sevenFifteenProject;
          this.sevenBillable = this.sevenFifteenBillable;
        }
        else
        {
          document.getElementById("7")!.style.background = this.backgroundColour[Math.floor(Math.random() * 5)][0] ;
          this.tasks[this.numTasks][0] = this.seven;
          // start time
          this.tasks[this.numTasks][1] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T07:00:00Z";
          // end time
          this.tasks[this.numTasks][2] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T07:15:00Z";
          // Setting the project
          this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1;
          this.projectID = this.activities[this.project][0];
          this.sevenProject = this.projectID ;
          this.tasks[this.numTasks][3] =  this.projectID ;
          this.sevenBillable = this.billable;
          this.tasks[this.numTasks][4] = this.sevenBillable;
        }

      }
    }
  }


  // setting the task for 7:15
  Clicked715():void{
    // Checking if the project is blank
    if ((document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1 === -1)
    {
      this.ErrorResult = "Project cannot be left blank";
      const x = document.getElementById("ErrorResultMessage")!;
      x.style.display = "block";
    }
    else
    {
      // Duplicating the task setting
      if ((document.getElementById("tasks") as HTMLInputElement).value === "")
      {
        (document.getElementById("tasks") as HTMLInputElement).value = this.sevenFifteen;
        for (let i = 0; i < this.k ; ++i)
        {
          if (this.activities[i][0] === this.sevenFifteenProject)
          {
            (document.getElementById("ProjectsList") as HTMLInputElement).value = this.activities[i][1];
          }
        }
        // Setting the billable status
        this.billable = this.sevenFifteenBillable;
        if (this.billable === "false")
        {
          document.getElementById("billable")!.style.color = "#000000";
        }
        else
        {
          document.getElementById("billable")!.style.color = "#3FA5F7";
        }
      }
      else
      {
        this.sevenFifteen = (document.getElementById("tasks") as HTMLInputElement).value;
        if (this.sevenFifteen === this.seven)
        {
          this.tasks[this.numTasks][2] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T07:30:00Z";
          document.getElementById("715")!.style.background = document.getElementById("7")!.style.backgroundColor;
          this.sevenFifteenProject = this.sevenProject;
          this.sevenFifteenBillable = this.sevenBillable;
        }
        else
        {
          if (this.sevenFifteen === this.sevenThirty)
          {
            document.getElementById("715")!.style.background = document.getElementById("730")!.style.backgroundColor;
            this.tasks[this.numTasks][1] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T07:15:00Z";
            this.sevenFifteenProject = this.sevenThirtyProject;
            this.sevenFifteenBillable = this.sevenThirtyBillable;
          }
          else
          {
            document.getElementById("715")!.style.background = this.backgroundColour[Math.floor(Math.random() * 5)][0] ;
            ++this.numTasks;
            // start time
            this.tasks[this.numTasks][0] = this.sevenFifteen;
            this.tasks[this.numTasks][1] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T07:15:00Z";
            // end time
            this.tasks[this.numTasks][2] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T07:30:00Z";
            // Setting the project
            this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1;
            this.projectID = this.activities[this.project][0];
            this.sevenFifteenProject = this.projectID ;
            this.tasks[this.numTasks][3] =  this.projectID ;
            this.sevenFifteenBillable = this.billable;
            this.tasks[this.numTasks][4] = this.sevenFifteenBillable;
          }
        }
      }
    }
  }


  // setting the task for 7:30
  Clicked730():void{
    // Checking if the project is blank
    if ((document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1 === -1)
    {
      this.ErrorResult = "Project cannot be left blank";
      const x = document.getElementById("ErrorResultMessage")!;
      x.style.display = "block";
    }
    else
    {
      // Duplicating the task setting
      if ((document.getElementById("tasks") as HTMLInputElement).value === "")
      {
        (document.getElementById("tasks") as HTMLInputElement).value = this.sevenThirty;
        for (let i = 0; i < this.k ; ++i)
        {
          if (this.activities[i][0] === this.sevenThirtyProject)
          {
            (document.getElementById("ProjectsList") as HTMLInputElement).value = this.activities[i][1];
          }
        }
        // Setting the billable status
        this.billable = this.sevenThirtyBillable;
        if (this.billable === "false")
        {
          document.getElementById("billable")!.style.color = "#000000";
        }
        else
        {
          document.getElementById("billable")!.style.color = "#3FA5F7";
        }
      }
      else
      {
        this.sevenThirty = (document.getElementById("tasks") as HTMLInputElement).value;
        if (this.sevenThirty === this.sevenFifteen)
        {
          this.tasks[this.numTasks][2] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T07:45:00Z";
          document.getElementById("730")!.style.background = document.getElementById("715")!.style.backgroundColor;
          this.sevenThirtyProject = this.sevenFifteenProject ;
          this.sevenThirtyBillable = this.sevenFifteenBillable;
        }
        else
        {
          if (this.sevenThirty === this.sevenFortyFive)
          {
            document.getElementById("730")!.style.background = document.getElementById("745")!.style.backgroundColor;
            this.tasks[this.numTasks][1] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T07:30:00Z";
            this.sevenThirtyProject = this.sevenFortyFiveProject;
            this.sevenThirtyBillable = this.sevenFortyFiveBillable;
          }
          else
          {
            document.getElementById("730")!.style.background = this.backgroundColour[Math.floor(Math.random() * 5)][0] ;
            ++this.numTasks;
            // description
            this.tasks[this.numTasks][0] = this.sevenThirty;
            // start Time
            this.tasks[this.numTasks][1] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T07:30:00Z";
            // end time
            this.tasks[this.numTasks][2] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T07:45:00Z";
            // Setting the project
            this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex -1;
            this.projectID = this.activities[this.project][0];
            this.sevenThirtyProject = this.projectID ;
            this.tasks[this.numTasks][3] =  this.projectID ;
            this.sevenThirtyBillable = this.billable;
            this.tasks[this.numTasks][4] = this.sevenThirtyBillable;
          }
        }
      }
    }
  }

  // setting the task for 7:45
  Clicked745():void{
    // Checking if the project is blank
    if ((document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1 === -1)
    {
      this.ErrorResult = "Project cannot be left blank";
      const x = document.getElementById("ErrorResultMessage")!;
      x.style.display = "block";
    }
    else
    {
      // Duplicating the task setting
      if ((document.getElementById("tasks") as HTMLInputElement).value === "")
      {
        (document.getElementById("tasks") as HTMLInputElement).value = this.sevenFortyFive;
        for (let i = 0; i < this.k ; ++i)
        {
          if (this.activities[i][0] === this.sevenFortyFiveProject)
          {
            (document.getElementById("ProjectsList") as HTMLInputElement).value = this.activities[i][1];
          }
        }
        // Setting the billable status
        this.billable = this.sevenFortyFiveBillable;
        if (this.billable === "false")
        {
          document.getElementById("billable")!.style.color = "#000000";
        }
        else
        {
          document.getElementById("billable")!.style.color = "#3FA5F7";
        }
      }
      else
      {
        this.sevenFortyFive = (document.getElementById("tasks") as HTMLInputElement).value;
        if (this.sevenFortyFive === this.sevenThirty)
        {
          this.tasks[this.numTasks][2] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T08:00:00Z";
          document.getElementById("745")!.style.background = document.getElementById("730")!.style.backgroundColor;
          this.sevenFortyFiveProject = this.sevenThirtyProject;
          this.sevenFortyFiveBillable = this.sevenThirtyBillable;
        }
        else
        {
          if (this.sevenFortyFive === this.eight)
          {
            document.getElementById("745")!.style.background = document.getElementById("8")!.style.backgroundColor;
            this.tasks[this.numTasks][1] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T07:45:00Z";
            this.sevenFortyFiveProject = this.eightProject;
            this.sevenFortyFiveBillable = this.eightBillable;
          }
          else
          {
           document.getElementById("745")!.style.background = this.backgroundColour[Math.floor(Math.random() * 5)][0] ;
            ++this.numTasks;
            // description
            this.tasks[this.numTasks][0] = this.sevenFortyFive;
            // start Time
            this.tasks[this.numTasks][1] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T07:45:00Z";
            // end time
            this.tasks[this.numTasks][2] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T08:00:00Z";
            // Setting the project
            this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1;
            this.projectID = this.activities[this.project][0];
            this.sevenFortyFiveProject = this.projectID ;
            this.tasks[this.numTasks][3] =  this.projectID ;
            this.sevenFortyFiveBillable = this.billable;
            this.tasks[this.numTasks][4] = this.sevenFortyFiveBillable;
          }
        }
      }
    }
  }


  // setting the task for 8:00
  Clicked8():void{
    // Checking if the project is blank
    if ((document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1 === -1)
    {
      this.ErrorResult = "Project cannot be left blank";
      const x = document.getElementById("ErrorResultMessage")!;
      x.style.display = "block";
    }
    else
    {
      // Duplicating the task setting
      if ((document.getElementById("tasks") as HTMLInputElement).value === "")
      {
        (document.getElementById("tasks") as HTMLInputElement).value = this.eight;
        for (let i = 0; i < this.k ; ++i)
        {
          if (this.activities[i][0] === this.eightProject)
          {
            (document.getElementById("ProjectsList") as HTMLInputElement).value = this.activities[i][1];
          }
        }
        // Setting the billable status
        this.billable = this.eightBillable;
        if (this.billable === "false")
        {
          document.getElementById("billable")!.style.color = "#000000";
        }
        else
        {
          document.getElementById("billable")!.style.color = "#3FA5F7";
        }
      }
      else
      {
        this.eight = (document.getElementById("tasks") as HTMLInputElement).value;
        if (this.eight === this.sevenFortyFive)
        {
          this.tasks[this.numTasks][2] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T08:15:00Z";
          document.getElementById("8")!.style.background = document.getElementById("745")!.style.backgroundColor;
          this.eightProject = this.sevenFortyFiveProject;
          this.eightBillable = this.sevenFortyFiveBillable;
        }
        else
        {
          if (this.eight === this.eightFifteen)
          {
            document.getElementById("8")!.style.background = document.getElementById("815")!.style.backgroundColor;
            this.tasks[this.numTasks][1] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T08:00:00Z";
            this.eightProject = this.eightFifteenProject;
            this.eightBillable = this.eightFifteenBillable;
          }
          else
          {
            document.getElementById("8")!.style.background = this.backgroundColour[Math.floor(Math.random() * 5)][0] ;
            ++this.numTasks;
            // description
            this.tasks[this.numTasks][0] = this.eight;
            // start Time
            this.tasks[this.numTasks][1] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T08:00:00Z";
            // end time
            this.tasks[this.numTasks][2] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T08:15:00Z";
            // Setting the project
            this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1;
            this.projectID = this.activities[this.project][0];
            this.eightProject = this.projectID ;
            this.tasks[this.numTasks][3] =  this.projectID ;
            this.eightBillable = this.billable;
            this.tasks[this.numTasks][4] = this.eightBillable;
          }
        }
      }
    }
  }


  // setting the task for 8:15
  Clicked815():void{
    // Checking if the project is blank
    if ((document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1 === -1)
    {
      this.ErrorResult = "Project cannot be left blank";
      const x = document.getElementById("ErrorResultMessage")!;
      x.style.display = "block";
    }
    else
    {
      // Duplicating the task setting
      if ((document.getElementById("tasks") as HTMLInputElement).value === "")
      {
        (document.getElementById("tasks") as HTMLInputElement).value = this.eightFifteen;
        for (let i = 0; i < this.k ; ++i)
        {
          if (this.activities[i][0] === this.eightFifteenProject)
          {
            (document.getElementById("ProjectsList") as HTMLInputElement).value = this.activities[i][1];
          }
        }
        // Setting the billable status
        this.billable = this.eightFifteenBillable;
        if (this.billable === "false")
        {
          document.getElementById("billable")!.style.color = "#000000";
        }
        else
        {
          document.getElementById("billable")!.style.color = "#3FA5F7";
        }
      }
      else
      {
        this.eightFifteen = (document.getElementById("tasks") as HTMLInputElement).value;
        if (this.eightFifteen === this.eight)
        {
          this.tasks[this.numTasks][2] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T08:30:00Z";
          document.getElementById("815")!.style.background = document.getElementById("8")!.style.backgroundColor;
          this.eightFifteenProject = this.eightProject;
          this.eightFifteenBillable = this.eightBillable;
        }
        else
        {
          if (this.eightFifteen === this.eightThirty)
          {
            document.getElementById("815")!.style.background = document.getElementById("830")!.style.backgroundColor;
            this.tasks[this.numTasks][1] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T08:15:00Z";
            this.eightFifteenProject = this.eightThirtyProject;
            this.eightFifteenBillable = this.eightThirtyBillable;
          }
          else
          {
            document.getElementById("815")!.style.background = this.backgroundColour[Math.floor(Math.random() * 5)][0] ;
            ++this.numTasks;
            // description
            this.tasks[this.numTasks][0] = this.eightFifteen;
            // start Time
            this.tasks[this.numTasks][1] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T08:15:00Z";
            // end time
            this.tasks[this.numTasks][2] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T08:30:00Z";
            // Setting the project
            this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1;
            this.projectID = this.activities[this.project][0];
            this.eightFifteenProject = this.projectID ;
            this.tasks[this.numTasks][3] =  this.projectID ;
            this.eightFifteenBillable = this.billable;
            this.tasks[this.numTasks][4] = this.eightFifteenBillable;
          }
        }
      }
    }
  }


  // setting the task for 8:30
  Clicked830():void{
    // Checking if the project is blank
    if ((document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1 === -1)
    {
      this.ErrorResult = "Project cannot be left blank";
      const x = document.getElementById("ErrorResultMessage")!;
      x.style.display = "block";
    }
    else
    {
      // Duplicating the task setting
      if ((document.getElementById("tasks") as HTMLInputElement).value === "")
      {
        (document.getElementById("tasks") as HTMLInputElement).value = this.eightThirty;
        for (let i = 0; i < this.k ; ++i)
        {
          if (this.activities[i][0] === this.eightThirtyProject)
          {
            (document.getElementById("ProjectsList") as HTMLInputElement).value = this.activities[i][1];
          }
        }
        // Setting the billable status
        this.billable = this.eightThirtyBillable;
        if (this.billable === "false")
        {
          document.getElementById("billable")!.style.color = "#000000";
        }
        else
        {
          document.getElementById("billable")!.style.color = "#3FA5F7";
        }
      }
      else
      {
        this.eightThirty = (document.getElementById("tasks") as HTMLInputElement).value;
        if (this.eightThirty === this.eightFifteen)
        {
          this.tasks[this.numTasks][2] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T08:45:00Z";
          document.getElementById("830")!.style.background = document.getElementById("815")!.style.backgroundColor;
          this.eightThirtyProject = this.eightFifteenProject;
          this.eightThirtyBillable = this.eightFifteenBillable ;
        }
        else
        {
          if (this.eightThirty === this.eightFortyFive)
          {
            document.getElementById("830")!.style.background = document.getElementById("845")!.style.backgroundColor;
            this.tasks[this.numTasks][1] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T08:30:00Z";
            this.eightThirtyProject = this.eightFortyFiveProject;
            this.eightThirtyBillable = this.eightFortyFiveBillable;
          }
          else
          {
            document.getElementById("830")!.style.background = this.backgroundColour[Math.floor(Math.random() * 5)][0] ;
            ++this.numTasks;
            // description
            this.tasks[this.numTasks][0] = this.eightThirty;
            // start Time
            this.tasks[this.numTasks][1] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T08:30:00Z";
            // end time
            this.tasks[this.numTasks][2] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T08:45:00Z";
            // Setting the project
            this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1;
            this.projectID = this.activities[this.project][0];
            this.eightThirtyProject = this.projectID ;
            this.tasks[this.numTasks][3] =  this.projectID ;
            this.eightThirtyBillable = this.billable;
            this.tasks[this.numTasks][4] = this.eightThirtyBillable;
          }
        }
      }
    }
  }


  // setting the task for 8:45
  Clicked845():void{
    // Checking if the project is blank
    if ((document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1 === -1)
    {
      this.ErrorResult = "Project cannot be left blank";
      const x = document.getElementById("ErrorResultMessage")!;
      x.style.display = "block";
    }
    else
    {
      // Duplicating the task setting
      if ((document.getElementById("tasks") as HTMLInputElement).value === "")
      {
        (document.getElementById("tasks") as HTMLInputElement).value = this.eightFortyFive;
        for (let i = 0; i < this.k ; ++i)
        {
          if (this.activities[i][0] === this.eightFortyFiveProject)
          {
            (document.getElementById("ProjectsList") as HTMLInputElement).value = this.activities[i][1];
          }
        }
        // Setting the billable status
        this.billable = this.eightFortyFiveBillable;
        if (this.billable === "false")
        {
          document.getElementById("billable")!.style.color = "#000000";
        }
        else
        {
          document.getElementById("billable")!.style.color = "#3FA5F7";
        }
      }
      else
      {
        this.eightFortyFive = (document.getElementById("tasks") as HTMLInputElement).value;
        if (this.eightFortyFive === this.eightThirty)
        {
          this.tasks[this.numTasks][2] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T09:00:00Z";
          document.getElementById("845")!.style.background = document.getElementById("830")!.style.backgroundColor;
          this.eightFortyFiveProject = this.eightThirtyProject;
          this.eightFortyFiveBillable = this.eightThirtyBillable;
        }
        else
        {
          if (this.eightFortyFive === this.nine)
          {
            document.getElementById("845")!.style.background = document.getElementById("9")!.style.backgroundColor;
            this.tasks[this.numTasks][1] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T08:45:00Z";
            this.eightFortyFiveProject = this.nineProject;
            this.eightFortyFiveBillable = this.nineBillable;
          }
          else
          {
            document.getElementById("845")!.style.background = this.backgroundColour[Math.floor(Math.random() * 5)][0] ;
            ++this.numTasks;
            // description
            this.tasks[this.numTasks][0] = this.eightFortyFive;
            // start Time
            this.tasks[this.numTasks][1] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T08:45:00Z";
            // end time
            this.tasks[this.numTasks][2] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T09:00:00Z";
            // Setting the project
            this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1;
            this.projectID = this.activities[this.project][0];
            this.eightFortyFiveProject = this.projectID ;
            this.tasks[this.numTasks][3] =  this.projectID ;
            this.eightFortyFiveBillable = this.billable;
            this.tasks[this.numTasks][4] = this.eightFortyFiveBillable;
          }
        }
      }
    }
  }


  // setting the task for 9:00
  Clicked9():void{
    // Checking if the project is blank
    if ((document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1 === -1)
    {
      this.ErrorResult = "Project cannot be left blank";
      const x = document.getElementById("ErrorResultMessage")!;
      x.style.display = "block";
    }
    else
    {
      // Duplicating the task setting
      if ((document.getElementById("tasks") as HTMLInputElement).value === "")
      {
        (document.getElementById("tasks") as HTMLInputElement).value = this.nine;
        for (let i = 0; i < this.k ; ++i)
        {
          if (this.activities[i][0] === this.nineProject)
          {
            (document.getElementById("ProjectsList") as HTMLInputElement).value = this.activities[i][1];
          }
        }
        // Setting the billable status
        this.billable = this.nineBillable;
        if (this.billable === "false")
        {
          document.getElementById("billable")!.style.color = "#000000";
        }
        else
        {
          document.getElementById("billable")!.style.color = "#3FA5F7";
        }
      }
      else
      {
        this.nine = (document.getElementById("tasks") as HTMLInputElement).value;
        if (this.nine === this.eightFortyFive)
        {
          this.tasks[this.numTasks][2] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T09:15:00Z";
          document.getElementById("9")!.style.background = document.getElementById("845")!.style.backgroundColor;
          this.nineProject = this.eightFortyFiveProject;
          this.nineBillable = this.eightFortyFiveBillable;
        }
        else
        {
          if (this.nine === this.nineFifteen)
          {
            document.getElementById("9")!.style.background = document.getElementById("915")!.style.backgroundColor;
            this.tasks[this.numTasks][1] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T09:00:00Z";
            this.nineProject = this.nineFifteenProject;
            this.nineBillable = this.nineFifteenBillable;
          }
          else
          {
            document.getElementById("9")!.style.background = this.backgroundColour[Math.floor(Math.random() * 5)][0] ;
            ++this.numTasks;
            // description
            this.tasks[this.numTasks][0] = this.nine;
            // start Time
            this.tasks[this.numTasks][1] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T09:00:00Z";
            // end time
            this.tasks[this.numTasks][2] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T09:15:00Z";
            // Setting the project
            this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1;
            this.projectID = this.activities[this.project][0];
            this.nineProject = this.projectID ;
            this.tasks[this.numTasks][3] =  this.projectID ;
            this.nineBillable = this.billable;
            this.tasks[this.numTasks][4] = this.nineBillable;
          }
        }
      }
    }
  }


  // setting the task for 9:15
  Clicked915():void{
    // Checking if the project is blank
    if ((document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1 === -1)
    {
      this.ErrorResult = "Project cannot be left blank";
      const x = document.getElementById("ErrorResultMessage")!;
      x.style.display = "block";
    }
    else
    {
      // Duplicating the task setting
      if ((document.getElementById("tasks") as HTMLInputElement).value === "")
      {
        (document.getElementById("tasks") as HTMLInputElement).value = this.nineFifteen;
        for (let i = 0; i < this.k ; ++i)
        {
          if (this.activities[i][0] === this.nineFifteenProject)
          {
            (document.getElementById("ProjectsList") as HTMLInputElement).value = this.activities[i][1];
          }
        }
        // Setting the billable status
        this.billable = this.nineFifteenBillable;
        if (this.billable === "false")
        {
          document.getElementById("billable")!.style.color = "#000000";
        }
        else
        {
          document.getElementById("billable")!.style.color = "#3FA5F7";
        }
      }
      else
      {
        this.nineFifteen = (document.getElementById("tasks") as HTMLInputElement).value;
        if (this.nineFifteen === this.nine)
        {
          this.tasks[this.numTasks][2] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T09:30:00Z";
          document.getElementById("915")!.style.background = document.getElementById("9")!.style.backgroundColor;
          this.nineFifteenProject  = this.nineProject;
          this.nineFifteenBillable = this.nineBillable;
        }
        else
        {
          if (this.nineFifteen === this.nineThirty)
          {
            document.getElementById("915")!.style.background = document.getElementById("930")!.style.backgroundColor;
            this.tasks[this.numTasks][1] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T09:15:00Z";
            this.nineFifteenProject = this.nineThirtyProject;
            this.nineFifteenBillable = this.nineThirtyBillable;
          }
          else
          {
            document.getElementById("915")!.style.background = this.backgroundColour[Math.floor(Math.random() * 5)][0] ;
            ++this.numTasks;
            // description
            this.tasks[this.numTasks][0] = this.nineFifteen;
            // start Time
            this.tasks[this.numTasks][1] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T09:15:00Z";
            // end time
            this.tasks[this.numTasks][2] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T09:30:00Z";
            // Setting the project
            this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1;
            this.projectID = this.activities[this.project][0];
            this.nineFifteenProject = this.projectID ;
            this.tasks[this.numTasks][3] =  this.projectID ;
            this.nineFifteenBillable = this.billable;
            this.tasks[this.numTasks][4] = this.nineFifteenBillable;
          }
        }
      }
    }
  }


  // setting the task for 9:30
  Clicked930():void{
    // Checking if the project is blank
    if ((document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1 === -1)
    {
      this.ErrorResult = "Project cannot be left blank";
      const x = document.getElementById("ErrorResultMessage")!;
      x.style.display = "block";
    }
    else
    {
      // Duplicating the task setting
      if ((document.getElementById("tasks") as HTMLInputElement).value === "")
      {
        (document.getElementById("tasks") as HTMLInputElement).value = this.nineThirty;
        for (let i = 0; i < this.k ; ++i)
        {
          if (this.activities[i][0] === this.nineThirtyProject)
          {
            (document.getElementById("ProjectsList") as HTMLInputElement).value = this.activities[i][1];
          }
        }
        // Setting the billable status
        this.billable = this.nineThirtyBillable;
        if (this.billable === "false")
        {
          document.getElementById("billable")!.style.color = "#000000";
        }
        else
        {
          document.getElementById("billable")!.style.color = "#3FA5F7";
        }
      }
      else
      {
        this.nineThirty = (document.getElementById("tasks") as HTMLInputElement).value;
        if (this.nineThirty === this.nineFifteen)
        {
          this.tasks[this.numTasks][2] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T09:45:00Z";
          document.getElementById("930")!.style.background = document.getElementById("915")!.style.backgroundColor;
          this.nineThirtyProject = this.nineFifteenProject;
          this.nineThirtyBillable = this.nineFifteenBillable;
        }
        else
        {
          if (this.nineThirty === this.nineFortyFive)
          {
            document.getElementById("930")!.style.background = document.getElementById("945")!.style.backgroundColor;
            this.tasks[this.numTasks][1] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T09:30:00Z";
            this.nineThirtyProject = this.nineFortyFiveProject;
            this.nineThirtyBillable = this.nineFortyFiveBillable;
          }
          else
          {
            document.getElementById("930")!.style.background = this.backgroundColour[Math.floor(Math.random() * 5)][0] ;
            ++this.numTasks;
            // description
            this.tasks[this.numTasks][0] = this.nineThirty;
            // start Time
            this.tasks[this.numTasks][1] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T09:30:00Z";
            // end time
            this.tasks[this.numTasks][2] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T09:45:00Z";
            // Setting the project
            this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1;
            this.projectID = this.activities[this.project][0];
            this.nineThirtyProject = this.projectID ;
            this.tasks[this.numTasks][3] =  this.projectID ;
            this.nineThirtyBillable = this.billable;
            this.tasks[this.numTasks][4] = this.nineThirtyBillable;
          }
        }
      }
    }
  }
  // setting the task for 9:45
  Clicked945():void{
    // Checking if the project is blank
    if ((document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1 === -1)
    {
      this.ErrorResult = "Project cannot be left blank";
      const x = document.getElementById("ErrorResultMessage")!;
      x.style.display = "block";
    }
    else
    {
      // Duplicating the task setting
      if ((document.getElementById("tasks") as HTMLInputElement).value === "")
      {
        (document.getElementById("tasks") as HTMLInputElement).value = this.nineFortyFive;
        for (let i = 0; i < this.k ; ++i)
        {
          if (this.activities[i][0] === this.nineFortyFiveProject)
          {
            (document.getElementById("ProjectsList") as HTMLInputElement).value = this.activities[i][1];
          }
        }
        // Setting the billable status
        this.billable = this.nineFortyFiveBillable;
        if (this.billable === "false")
        {
          document.getElementById("billable")!.style.color = "#000000";
        }
        else
        {
          document.getElementById("billable")!.style.color = "#3FA5F7";
        }
      }
      else
      {
        this.nineFortyFive = (document.getElementById("tasks") as HTMLInputElement).value;
        if (this.nineFortyFive === this.nineThirty)
        {
          this.tasks[this.numTasks][2] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T10:00:00Z";
          document.getElementById("945")!.style.background = document.getElementById("930")!.style.backgroundColor;
          this.nineFortyFiveProject = this.nineThirtyProject;
          this.nineFortyFiveBillable = this.nineThirtyBillable;
        }
        else
        {
          if (this.nineFortyFive === this.ten)
          {
            document.getElementById("945")!.style.background = document.getElementById("10")!.style.backgroundColor;
            this.tasks[this.numTasks][1] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T09:45:00Z";
            this.nineFortyFiveProject = this.tenProject;
            this.nineFortyFiveBillable = this.tenBillable;
          }
          else
          {
            document.getElementById("945")!.style.background = this.backgroundColour[Math.floor(Math.random() * 5)][0] ;
            ++this.numTasks;
            // description
            this.tasks[this.numTasks][0] = this.nineFortyFive;
            // start Time
            this.tasks[this.numTasks][1] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T09:45:00Z";
            // end time
            this.tasks[this.numTasks][2] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T10:00:00Z";
            // Setting the project
            this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1;
            this.projectID = this.activities[this.project][0];
            this.nineFortyFiveProject = this.projectID ;
            this.tasks[this.numTasks][3] =  this.projectID ;
            this.nineFortyFiveBillable = this.billable;
            this.tasks[this.numTasks][4] = this.nineFortyFiveBillable;
          }
        }
      }
    }
  }

  // setting the task for 10:00
  Clicked10():void{
    // Checking if the project is blank
    if ((document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1 === -1)
    {
      this.ErrorResult = "Project cannot be left blank";
      const x = document.getElementById("ErrorResultMessage")!;
      x.style.display = "block";
    }
    else
    {
      // Duplicating the task setting
      if ((document.getElementById("tasks") as HTMLInputElement).value === "")
      {
        (document.getElementById("tasks") as HTMLInputElement).value = this.ten;
        for (let i = 0; i < this.k ; ++i)
        {
          if (this.activities[i][0] === this.tenProject)
          {
            (document.getElementById("ProjectsList") as HTMLInputElement).value = this.activities[i][1];
          }
        }
        // Setting the billable status
        this.billable = this.tenBillable;
        if (this.billable === "false")
        {
          document.getElementById("billable")!.style.color = "#000000";
        }
        else
        {
          document.getElementById("billable")!.style.color = "#3FA5F7";
        }
      }
      else
      {
        this.ten = (document.getElementById("tasks") as HTMLInputElement).value;
        if (this.ten === this.nineFortyFive)
        {
          this.tasks[this.numTasks][2] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T10:15:00Z";
          document.getElementById("10")!.style.background = document.getElementById("945")!.style.backgroundColor;
          this.tenProject = this.nineFortyFiveProject;
          this.tenBillable = this.nineFortyFiveBillable;
        }
        else
        {
          if (this.ten === this.tenFifteen)
          {
            document.getElementById("10")!.style.background = document.getElementById("1015")!.style.backgroundColor;
            this.tasks[this.numTasks][1] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T10:00:00Z";
            this.tenProject = this.tenFifteenProject;
            this.tenBillable = this.tenFifteenBillable;

          }
          else
          {
            document.getElementById("10")!.style.background = this.backgroundColour[Math.floor(Math.random() * 5)][0] ;
            ++this.numTasks;
            // description
            this.tasks[this.numTasks][0] = this.ten;
            // start Time
            this.tasks[this.numTasks][1] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T10:00:00Z";
            // end time
            this.tasks[this.numTasks][2] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T10:15:00Z";
            // Setting the project
            this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1;
            this.projectID = this.activities[this.project][0];
            this.tenProject = this.projectID ;
            this.tasks[this.numTasks][3] =  this.projectID ;
            this.tenBillable = this.billable;
            this.tasks[this.numTasks][4] = this.tenBillable;
          }
        }
      }
    }
  }

  // setting the task for 10:15
  Clicked1015():void{
    // Checking if the project is blank
    if ((document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1 === -1)
    {
      this.ErrorResult = "Project cannot be left blank";
      const x = document.getElementById("ErrorResultMessage")!;
      x.style.display = "block";
    }
    else
    {
      // Duplicating the task setting
      if ((document.getElementById("tasks") as HTMLInputElement).value === "")
      {
        (document.getElementById("tasks") as HTMLInputElement).value = this.tenFifteen;
        for (let i = 0; i < this.k ; ++i)
        {
          if (this.activities[i][0] === this.tenFifteenProject)
          {
            (document.getElementById("ProjectsList") as HTMLInputElement).value = this.activities[i][1];
          }
        }
        // Setting the billable status
        this.billable = this.tenFifteenBillable;
        if (this.billable === "false")
        {
          document.getElementById("billable")!.style.color = "#000000";
        }
        else
        {
          document.getElementById("billable")!.style.color = "#3FA5F7";
        }
      }
      else
      {
        this.tenFifteen = (document.getElementById("tasks") as HTMLInputElement).value;
        if (this.tenFifteen === this.ten)
        {
          this.tasks[this.numTasks][2] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T10:30:00Z";
          document.getElementById("1015")!.style.background = document.getElementById("10")!.style.backgroundColor;
          this.tenFifteenProject  = this.tenProject;
          this.tenFifteenBillable = this.tenBillable;
        }
        else
        {
          if (this.tenFifteen === this.tenThirty)
          {
            document.getElementById("1015")!.style.background = document.getElementById("1030")!.style.backgroundColor;
            this.tasks[this.numTasks][1] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T10:15:00Z";
            this.tenFifteenProject  = this.tenThirtyProject;
            this.tenFifteenBillable = this.tenThirtyBillable;
          }
          else
          {
            document.getElementById("1015")!.style.background = this.backgroundColour[Math.floor(Math.random() * 5)][0] ;
            ++this.numTasks;
            // description
            this.tasks[this.numTasks][0] = this.tenFifteen;
            // start Time
            this.tasks[this.numTasks][1] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T10:15:00Z";
            // end time
            this.tasks[this.numTasks][2] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T10:30:00Z";
            // Setting the project
            this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1;
            this.projectID = this.activities[this.project][0];
            this.tenFifteenProject = this.projectID ;
            this.tasks[this.numTasks][3] =  this.projectID ;
            this.tenFifteenBillable = this.billable;
            this.tasks[this.numTasks][4] = this.tenFifteenBillable;
          }
        }
      }
    }
  }

  // setting the task for 10:30
  Clicked1030():void{
    // Checking if the project is blank
    if ((document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1 === -1)
    {
      this.ErrorResult = "Project cannot be left blank";
      const x = document.getElementById("ErrorResultMessage")!;
      x.style.display = "block";
    }
    else
    {
      // Duplicating the task setting
      if ((document.getElementById("tasks") as HTMLInputElement).value === "")
      {
        (document.getElementById("tasks") as HTMLInputElement).value = this.tenThirty;
        for (let i = 0; i < this.k ; ++i)
        {
          if (this.activities[i][0] === this.tenThirtyProject)
          {
            (document.getElementById("ProjectsList") as HTMLInputElement).value = this.activities[i][1];
          }
        }
        // Setting the billable status
        this.billable = this.tenThirtyBillable;
        if (this.billable === "false")
        {
          document.getElementById("billable")!.style.color = "#000000";
        }
        else
        {
          document.getElementById("billable")!.style.color = "#3FA5F7";
        }
      }
      else
      {
        this.tenThirty = (document.getElementById("tasks") as HTMLInputElement).value;
        if (this.tenThirty === this.tenFifteen)
        {
          this.tasks[this.numTasks][2] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T10:45:00Z";
          document.getElementById("1030")!.style.background = document.getElementById("1015")!.style.backgroundColor;
          this.tenThirtyProject = this.tenFifteenProject;
          this.tenThirtyBillable = this.tenFifteenBillable;
        }
        else
        {
          if (this.tenThirty === this.tenFortyFive)
          {
            document.getElementById("1030")!.style.background = document.getElementById("1045")!.style.backgroundColor;
            this.tasks[this.numTasks][1] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T10:30:00Z";
            this.tenThirtyProject = this.tenFortyFiveProject;
            this.tenThirtyBillable = this.tenFortyFiveBillable;
          }
          else
          {
            document.getElementById("1030")!.style.background = this.backgroundColour[Math.floor(Math.random() * 5)][0] ;
            ++this.numTasks;
            // description
            this.tasks[this.numTasks][0] = this.tenThirty;
            // start Time
            this.tasks[this.numTasks][1] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T10:30:00Z";
            // end time
            this.tasks[this.numTasks][2] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T10:45:00Z";
            // Setting the project
            this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1;
            this.projectID = this.activities[this.project][0];
            this.tenThirtyProject = this.projectID ;
            this.tasks[this.numTasks][3] =  this.projectID ;
            this.tenThirtyBillable = this.billable;
            this.tasks[this.numTasks][4] = this.tenThirtyBillable;
          }
        }
      }
    }
  }


  // setting the task for 10:45
  Clicked1045():void{
    // Checking if the project is blank
    if ((document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1 === -1)
    {
      this.ErrorResult = "Project cannot be left blank";
      const x = document.getElementById("ErrorResultMessage")!;
      x.style.display = "block";
    }
    else
    {
      // Duplicating the task setting
      if ((document.getElementById("tasks") as HTMLInputElement).value === "")
      {
        (document.getElementById("tasks") as HTMLInputElement).value = this.tenFortyFive;
        for (let i = 0; i < this.k ; ++i)
        {
          if (this.activities[i][0] === this.tenFortyFiveProject)
          {
            (document.getElementById("ProjectsList") as HTMLInputElement).value = this.activities[i][1];
          }
        }
        // Setting the billable status
        this.billable = this.tenFortyFiveBillable;
        if (this.billable === "false")
        {
          document.getElementById("billable")!.style.color = "#000000";
        }
        else
        {
          document.getElementById("billable")!.style.color = "#3FA5F7";
        }
      }
      else
      {
        this.tenFortyFive = (document.getElementById("tasks") as HTMLInputElement).value;
        if (this.tenFortyFive === this.tenThirty)
        {
          this.tasks[this.numTasks][2] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T11:00:00Z";
          document.getElementById("1045")!.style.background = document.getElementById("1030")!.style.backgroundColor;
          this.tenFortyFiveProject = this.tenThirtyProject;
          this.tenFortyFiveBillable = this.tenThirtyBillable;
        }
        else
        {
          if (this.tenFortyFive === this.eleven)
          {
            document.getElementById("1045")!.style.background = document.getElementById("11")!.style.backgroundColor;
            this.tasks[this.numTasks][1] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T10:45:00Z";
            this.tenFortyFiveProject = this.elevenProject;
            this.tenFortyFiveBillable = this.elevenBillable;
          }
          else
          {
            document.getElementById("1045")!.style.background = this.backgroundColour[Math.floor(Math.random() * 5)][0] ;
            ++this.numTasks;
            // description
            this.tasks[this.numTasks][0] = this.tenFortyFive;
            // start Time
            this.tasks[this.numTasks][1] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T10:45:00Z";
            // end time
            this.tasks[this.numTasks][2] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T11:00:00Z";
            // Setting the project
            this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1;
            this.projectID = this.activities[this.project][0];
            this.tenFortyFiveProject = this.projectID ;
            this.tasks[this.numTasks][3] =  this.projectID ;
            this.tenFortyFiveBillable = this.billable;
            this.tasks[this.numTasks][4] = this.tenFortyFiveBillable;
          }
        }
      }
    }
  }

  // setting the task for 11:00
  Clicked11():void{
    // Checking if the project is blank
    if ((document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1 === -1)
    {
      this.ErrorResult = "Project cannot be left blank";
      const x = document.getElementById("ErrorResultMessage")!;
      x.style.display = "block";
    }
    else
    {
      // Duplicating the task setting
      if ((document.getElementById("tasks") as HTMLInputElement).value === "")
      {
        (document.getElementById("tasks") as HTMLInputElement).value = this.eleven;
        for (let i = 0; i < this.k ; ++i)
        {
          if (this.activities[i][0] === this.elevenProject)
          {
            (document.getElementById("ProjectsList") as HTMLInputElement).value = this.activities[i][1];
          }
        }
        // Setting the billable status
        this.billable = this.elevenBillable;
        if (this.billable === "false")
        {
          document.getElementById("billable")!.style.color = "#000000";
        }
        else
        {
          document.getElementById("billable")!.style.color = "#3FA5F7";
        }
      }
      else
      {
        this.eleven = (document.getElementById("tasks") as HTMLInputElement).value;
        if (this.eleven === this.tenFortyFive)
        {
          this.tasks[this.numTasks][2] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T11:15:00Z";
          document.getElementById("11")!.style.background = document.getElementById("1045")!.style.backgroundColor;
          this.elevenProject = this.tenFortyFiveProject;
          this.elevenBillable = this.tenFortyFiveBillable;
        }
        else
        {
          if (this.eleven === this.elevenFifteen)
          {
            document.getElementById("11")!.style.background = document.getElementById("1115")!.style.backgroundColor;
            this.tasks[this.numTasks][1] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T11:00:00Z";
            this.elevenProject = this.elevenFifteenProject;
            this.elevenBillable = this.elevenFifteenBillable;
          }
          else
          {
            document.getElementById("11")!.style.background = this.backgroundColour[Math.floor(Math.random() * 5)][0] ;
            ++this.numTasks;
            // description
            this.tasks[this.numTasks][0] = this.eleven;
            // start Time
            this.tasks[this.numTasks][1] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T11:00:00Z";
            // end time
            this.tasks[this.numTasks][2] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T11:15:00Z";
            // Setting the project
            this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1;
            this.projectID = this.activities[this.project][0];
            this.elevenProject = this.projectID ;
            this.tasks[this.numTasks][3] =  this.projectID ;
            this.elevenBillable = this.billable;
            this.tasks[this.numTasks][4] = this.elevenBillable;
          }
        }
      }
    }
  }

  // setting the task for 11:15
  Clicked1115():void{
    // Checking if the project is blank
    if ((document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1 === -1)
    {
      this.ErrorResult = "Project cannot be left blank";
      const x = document.getElementById("ErrorResultMessage")!;
      x.style.display = "block";
    }
    else
    {
      // Duplicating the task setting
      if ((document.getElementById("tasks") as HTMLInputElement).value === "")
      {
        (document.getElementById("tasks") as HTMLInputElement).value = this.elevenFifteen;
        for (let i = 0; i < this.k ; ++i)
        {
          if (this.activities[i][0] === this.elevenFifteenProject)
          {
            (document.getElementById("ProjectsList") as HTMLInputElement).value = this.activities[i][1];
          }
        }
        // Setting the billable status
        this.billable = this.elevenFifteenBillable;
        if (this.billable === "false")
        {
          document.getElementById("billable")!.style.color = "#000000";
        }
        else
        {
          document.getElementById("billable")!.style.color = "#3FA5F7";
        }
      }
      else
      {
        this.elevenFifteen = (document.getElementById("tasks") as HTMLInputElement).value;
        if (this.elevenFifteen === this.eleven)
        {
          this.tasks[this.numTasks][2] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T11:30:00Z";
          document.getElementById("1115")!.style.background = document.getElementById("11")!.style.backgroundColor;
          this.elevenFifteenProject  = this.elevenProject;
          this.elevenFifteenBillable = this.elevenBillable;
        }
        else
        {
          if (this.elevenFifteen === this.elevenThirty)
          {
            document.getElementById("1115")!.style.background = document.getElementById("1130")!.style.backgroundColor;
            this.tasks[this.numTasks][1] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T11:15:00Z";
            this.elevenFifteenProject  = this.elevenThirtyProject;
            this.elevenFifteenBillable = this.elevenThirtyBillable;
          }
          else
          {
            document.getElementById("1115")!.style.background = this.backgroundColour[Math.floor(Math.random() * 5)][0] ;
            ++this.numTasks;
            // description
            this.tasks[this.numTasks][0] = this.elevenFifteen;
            // start Time
            this.tasks[this.numTasks][1] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T11:15:00Z";
            // end time
            this.tasks[this.numTasks][2] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T11:30:00Z";
            // Setting the project
            this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1;
            this.projectID = this.activities[this.project][0];
            this.elevenFifteenProject = this.projectID ;
            this.tasks[this.numTasks][3] =  this.projectID ;
            this.elevenFifteenBillable = this.billable;
            this.tasks[this.numTasks][4] = this.elevenFifteenBillable;
          }
        }
      }
    }
  }

  // setting the task for 11:30
  Clicked1130():void{
    // Checking if the project is blank
    if ((document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1 === -1)
    {
      this.ErrorResult = "Project cannot be left blank";
      const x = document.getElementById("ErrorResultMessage")!;
      x.style.display = "block";
    }
    else
    {
      // Duplicating the task setting
      if ((document.getElementById("tasks") as HTMLInputElement).value === "")
      {
        (document.getElementById("tasks") as HTMLInputElement).value = this.elevenThirty;
        for (let i = 0; i < this.k ; ++i)
        {
          if (this.activities[i][0] === this.elevenThirtyProject)
          {
            (document.getElementById("ProjectsList") as HTMLInputElement).value = this.activities[i][1];
          }
        }
        // Setting the billable status
        this.billable = this.elevenThirtyBillable;
        if (this.billable === "false")
        {
          document.getElementById("billable")!.style.color = "#000000";
        }
        else
        {
          document.getElementById("billable")!.style.color = "#3FA5F7";
        }
      }
      else
      {
        this.elevenThirty = (document.getElementById("tasks") as HTMLInputElement).value;
        if (this.elevenThirty === this.elevenFifteen)
        {
          this.tasks[this.numTasks][2] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T11:45:00Z";
          document.getElementById("1130")!.style.background = document.getElementById("1115")!.style.backgroundColor;
          this.elevenThirtyProject = this.elevenFifteenProject;
          this.elevenThirtyBillable = this.elevenFifteenBillable;
        }
        else
        {
          if (this.elevenThirty === this.elevenFortyFive)
          {
            document.getElementById("1130")!.style.background = document.getElementById("1145")!.style.backgroundColor;
            this.tasks[this.numTasks][1] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T11:30:00Z";
            this.elevenThirtyProject = this.elevenFortyFiveProject;
            this.elevenThirtyBillable = this.elevenFortyFiveBillable;
          }
          else
          {
            document.getElementById("1130")!.style.background = this.backgroundColour[Math.floor(Math.random() * 5)][0] ;
            ++this.numTasks;
            // description
            this.tasks[this.numTasks][0] = this.elevenThirty;
            // start Time
            this.tasks[this.numTasks][1] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T11:30:00Z";
            // end time
            this.tasks[this.numTasks][2] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T11:45:00Z";
            // Setting the project
            this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1;
            this.projectID = this.activities[this.project][0];
            this.elevenThirtyProject = this.projectID ;
            this.tasks[this.numTasks][3] =  this.projectID ;
            this.elevenThirtyBillable = this.billable;
            this.tasks[this.numTasks][4] = this.elevenThirtyBillable;
          }
        }
      }
    }
  }

  // setting the task for 11:45
  Clicked1145():void{
    // Checking if the project is blank
    if ((document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1 === -1)
    {
      this.ErrorResult = "Project cannot be left blank";
      const x = document.getElementById("ErrorResultMessage")!;
      x.style.display = "block";
    }
    else
    {
      // Duplicating the task setting
      if ((document.getElementById("tasks") as HTMLInputElement).value === "")
      {
        (document.getElementById("tasks") as HTMLInputElement).value = this.elevenFortyFive;
        for (let i = 0; i < this.k ; ++i)
        {
          if (this.activities[i][0] === this.elevenFortyFiveProject)
          {
            (document.getElementById("ProjectsList") as HTMLInputElement).value = this.activities[i][1];
          }
        }
        // Setting the billable status
        this.billable = this.elevenFortyFiveBillable;
        if (this.billable === "false")
        {
          document.getElementById("billable")!.style.color = "#000000";
        }
        else
        {
          document.getElementById("billable")!.style.color = "#3FA5F7";
        }
      }
      else
      {
        this.elevenFortyFive = (document.getElementById("tasks") as HTMLInputElement).value;
        if (this.elevenFortyFive === this.elevenThirty)
        {
          this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T12:00:00Z";
          document.getElementById("1145")!.style.background = document.getElementById("1130")!.style.backgroundColor;
          this.elevenFortyFiveProject = this.elevenThirtyProject;
          this.elevenFortyFiveBillable = this.elevenThirtyBillable;
        }
        else
        {
          if (this.elevenFortyFive === this.twelve)
          {
            document.getElementById("1145")!.style.background = document.getElementById("12")!.style.backgroundColor;
            this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T11:45:00Z";
            this.elevenFortyFiveProject = this.twelveProject;
            this.elevenFortyFiveBillable = this.twelveBillable;
          }
          else
          {
            document.getElementById("1145")!.style.background = this.backgroundColour[Math.floor(Math.random() * 5)][0] ;
            ++this.numTasks;
            // description
            this.tasks[this.numTasks][0] = this.elevenFortyFive;
            // start Time
            this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T11:45:00Z";
            // end time
            this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T12:00:00Z";
            // Setting the project
            this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1;
            this.projectID = this.activities[this.project][0];
            this.elevenFortyFiveProject = this.projectID ;
            this.tasks[this.numTasks][3] =  this.projectID ;
            this.elevenFortyFiveBillable = this.billable;
            this.tasks[this.numTasks][4] = this.elevenFortyFiveBillable;
          }
        }
      }
    }
  }

  // setting the task for 12:00
  Clicked12():void{
    // Checking if the project is blank
    if ((document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1 === -1)
    {
      this.ErrorResult = "Project cannot be left blank";
      const x = document.getElementById("ErrorResultMessage")!;
      x.style.display = "block";
    }
    else
    {
      // Duplicating the task setting
      if ((document.getElementById("tasks") as HTMLInputElement).value === "")
      {
        (document.getElementById("tasks") as HTMLInputElement).value = this.twelve;
        for (let i = 0; i < this.k ; ++i)
        {
          if (this.activities[i][0] === this.twelveProject)
          {
            (document.getElementById("ProjectsList") as HTMLInputElement).value = this.activities[i][1];
          }
        }
        // Setting the billable status
        this.billable = this.twelveBillable;
        if (this.billable === "false")
        {
          document.getElementById("billable")!.style.color = "#000000";
        }
        else
        {
          document.getElementById("billable")!.style.color = "#3FA5F7";
        }
      }
      else
      {
        this.twelve = (document.getElementById("tasks") as HTMLInputElement).value;
        if (this.twelve === this.elevenFortyFive)
        {
          this.tasks[this.numTasks][2] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T12:15:00Z";
          document.getElementById("12")!.style.background = document.getElementById("1145")!.style.backgroundColor;
          this.twelveProject = this.elevenFortyFiveProject;
          this.twelveBillable = this.elevenFortyFiveBillable;
        }
        else
        {
          if (this.twelve === this.twelveFifteen)
          {
            document.getElementById("12")!.style.background = document.getElementById("1215")!.style.backgroundColor;
            this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T12:00:00Z";
            this.twelveProject = this.twelveFifteenProject;
            this.twelveBillable = this.twelveFifteenBillable;
          }
          else
          {
            document.getElementById("12")!.style.background = this.backgroundColour[Math.floor(Math.random() * 5)][0] ;
            ++this.numTasks;
            // description
            this.tasks[this.numTasks][0] = this.twelve;
            // start Time
            this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T12:00:00Z";
            // end time
            this.tasks[this.numTasks][2] = (document.getElementById("DatePicked")as HTMLInputElement).value + "T12:15:00Z";
            // Setting the project
            this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1;
            this.projectID = this.activities[this.project][0];
            this.twelveProject = this.projectID ;
            this.tasks[this.numTasks][3] =  this.projectID ;
            this.twelveBillable = this.billable;
            this.tasks[this.numTasks][4] = this.twelveBillable;
          }
        }
      }
    }
  }

  // setting the task for 12:15
  Clicked1215():void{
    // Checking if the project is blank
    if ((document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1 === -1)
    {
      this.ErrorResult = "Project cannot be left blank";
      const x = document.getElementById("ErrorResultMessage")!;
      x.style.display = "block";
    }
    else
    {
      // Duplicating the task setting
      if ((document.getElementById("tasks") as HTMLInputElement).value === "")
      {
        (document.getElementById("tasks") as HTMLInputElement).value = this.twelveFifteen;
        for (let i = 0; i < this.k ; ++i)
        {
          if (this.activities[i][0] === this.twelveFifteenProject)
          {
            (document.getElementById("ProjectsList") as HTMLInputElement).value = this.activities[i][1];
          }
        }
        // Setting the billable status
        this.billable = this.twelveFifteenBillable;
        if (this.billable === "false")
        {
          document.getElementById("billable")!.style.color = "#000000";
        }
        else
        {
          document.getElementById("billable")!.style.color = "#3FA5F7";
        }
      }
      else
      {
        this.twelveFifteen = (document.getElementById("tasks") as HTMLInputElement).value;
        if (this.twelveFifteen === this.twelve)
        {
          this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T12:30:00Z";
          document.getElementById("1215")!.style.background = document.getElementById("12")!.style.backgroundColor;
          this.twelveFifteenProject = this.twelveProject;
          this.twelveFifteenBillable = this.twelveBillable;
        }
        else
        {
          if (this.twelveFifteen === this.twelveThirty)
          {
            document.getElementById("1215")!.style.background = document.getElementById("1230")!.style.backgroundColor;
            this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T12:15:00Z";
            this.twelveFifteenProject = this.twelveThirtyProject;
            this.twelveFifteenBillable = this.twelveThirtyBillable;
          }
          else
          {
            document.getElementById("1215")!.style.background = this.backgroundColour[Math.floor(Math.random() * 5)][0] ;
            ++this.numTasks;
            // description
            this.tasks[this.numTasks][0] = this.twelveFifteen;
            // start Time
            this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T12:15:00Z";
            // end time
            this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T12:30:00Z";
            // Setting the project
            this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1;
            this.projectID = this.activities[this.project][0];
            this.twelveFifteenProject = this.projectID ;
            this.tasks[this.numTasks][3] =  this.projectID ;
            this.twelveFifteenBillable = this.billable;
            this.tasks[this.numTasks][4] = this.twelveFifteenBillable;
          }
        }
      }
    }
  }

  // setting the task for 12:30
  Clicked1230():void{
      // Checking if the project is blank
      if ((document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1 === -1)
      {
        this.ErrorResult = "Project cannot be left blank";
        const x = document.getElementById("ErrorResultMessage")!;
        x.style.display = "block";
      }
      else
      {
        // Duplicating the task setting
        if ((document.getElementById("tasks") as HTMLInputElement).value === "")
        {
          (document.getElementById("tasks") as HTMLInputElement).value = this.twelveThirty;
          for (let i = 0; i < this.k ; ++i)
          {
            if (this.activities[i][0] === this.twelveThirtyProject)
            {
              (document.getElementById("ProjectsList") as HTMLInputElement).value = this.activities[i][1];
            }
          }
          // Setting the billable status
          this.billable = this.twelveThirtyBillable;
          if (this.billable === "false")
          {
            document.getElementById("billable")!.style.color = "#000000";
          }
          else
          {
            document.getElementById("billable")!.style.color = "#3FA5F7";
          }
        }
        else
        {
          this.twelveThirty = (document.getElementById("tasks") as HTMLInputElement).value;
          if (this.twelveThirty === this.twelveFifteen)
          {
            this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T12:45:00Z";
            document.getElementById("1230")!.style.background = document.getElementById("1215")!.style.backgroundColor;
            this.twelveThirtyProject = this.twelveFifteenProject;
            this.twelveThirtyBillable = this.twelveFifteenBillable;
          }
          else
          {
            if (this.twelveThirty === this.twelveFortyFive)
            {
              document.getElementById("1230")!.style.background = document.getElementById("1245")!.style.backgroundColor;
              this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T12:30:00Z";
              this.twelveThirtyProject = this.twelveFortyFiveProject;
              this.twelveThirtyBillable = this.twelveFortyFiveBillable;
            }
            else
            {
              document.getElementById("1230")!.style.background = this.backgroundColour[Math.floor(Math.random() * 5)][0] ;
              ++this.numTasks;
              // description
              this.tasks[this.numTasks][0] = this.twelveThirty;
              // start Time
              this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value+ "T12:30:00Z";
              // end time
              this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T12:45:00Z";
              // Setting the project
              this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1;
              this.projectID = this.activities[this.project][0];
              this.twelveThirtyProject = this.projectID ;
              this.tasks[this.numTasks][3] =  this.projectID ;
              this.twelveThirtyBillable = this.billable;
              this.tasks[this.numTasks][4] = this.twelveThirtyBillable;
            }
          }
        }
      }
    }

  // setting the task for 12:45
    Clicked1245():void{
      // Checking if the project is blank
      if ((document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1 === -1)
      {
        this.ErrorResult = "Project cannot be left blank";
        const x = document.getElementById("ErrorResultMessage")!;
        x.style.display = "block";
      }
      else
      {
        // Duplicating the task setting
        if ((document.getElementById("tasks") as HTMLInputElement).value === "")
        {
          (document.getElementById("tasks") as HTMLInputElement).value = this.twelveFortyFive;
          for (let i = 0; i < this.k ; ++i)
          {
            if (this.activities[i][0] === this.twelveFortyFiveProject)
            {
              (document.getElementById("ProjectsList") as HTMLInputElement).value = this.activities[i][1];
            }
          }
          // Setting the billable status
          this.billable = this.twelveFortyFiveBillable;
          if (this.billable === "false")
          {
            document.getElementById("billable")!.style.color = "#000000";
          }
          else
          {
            document.getElementById("billable")!.style.color = "#3FA5F7";
          }
        }
        else
        {
          this.twelveFortyFive = (document.getElementById("tasks") as HTMLInputElement).value;
          if (this.twelveFortyFive === this.twelveThirty)
          {
            this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value+ "T13:00:00Z";
            document.getElementById("1245")!.style.background = document.getElementById("1230")!.style.backgroundColor;
            this.twelveFortyFiveProject =  this.twelveThirtyProject;
            this.twelveFortyFiveBillable = this.twelveThirtyBillable;
          }
          else
          {
            if (this.twelveFortyFive === this.thirteen)
            {
              document.getElementById("1245")!.style.background = document.getElementById("13")!.style.backgroundColor;
              this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T12:45:00Z";
              this.twelveFortyFiveProject = this.thirteenProject;
              this.twelveFortyFiveBillable = this.thirteenBillable;
            }
            else
            {
              document.getElementById("1245")!.style.background = this.backgroundColour[Math.floor(Math.random() * 5)][0];
              ++this.numTasks;
              // description
              this.tasks[this.numTasks][0] = this.twelveFortyFive;
              // start Time
              this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T12:45:00Z";
              // end time
              this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T13:00:00Z";
              // Setting the project
              this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1;
              this.projectID = this.activities[this.project][0];
              this.twelveFortyFiveProject = this.projectID ;
              this.tasks[this.numTasks][3] =  this.projectID ;
              this.twelveFortyFiveBillable = this.billable;
              this.tasks[this.numTasks][4] = this.twelveFortyFiveBillable;
            }
          }
        }
      }
    }

  // setting the task for 13:00
    Clicked13():void{
      // Checking if the project is blank
      if ((document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1 === -1)
      {
        this.ErrorResult = "Project cannot be left blank";
        const x = document.getElementById("ErrorResultMessage")!;
        x.style.display = "block";
      }
      else
      {
        // Duplicating the task setting
        if ((document.getElementById("tasks") as HTMLInputElement).value === "")
        {
          (document.getElementById("tasks") as HTMLInputElement).value = this.thirteen;
          for (let i = 0; i < this.k ; ++i)
          {
            if (this.activities[i][0] === this.thirteenProject)
            {
              (document.getElementById("ProjectsList") as HTMLInputElement).value = this.activities[i][1];
            }
          }
          // Setting the billable status
          this.billable = this.thirteenBillable;
          if (this.billable === "false")
          {
            document.getElementById("billable")!.style.color = "#000000";
          }
          else
          {
            document.getElementById("billable")!.style.color = "#3FA5F7";
          }
        }
        else
        {
          this.thirteen = (document.getElementById("tasks") as HTMLInputElement).value;
          if (this.thirteen === this.twelveFortyFive)
          {
            this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T13:15:00Z";
            document.getElementById("13")!.style.background = document.getElementById("1245")!.style.backgroundColor;
            this.thirteenProject = this.twelveFortyFiveProject;
            this.thirteenBillable = this.twelveFortyFiveBillable;
          }
          else
          {
            if (this.thirteen === this.thirteenFifteen)
            {
              document.getElementById("13")!.style.background = document.getElementById("1315")!.style.backgroundColor;
              this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value+ "T13:00:00Z";
              this.thirteenProject = this.thirteenFifteenProject;
              this.thirteenBillable = this.thirteenFifteenBillable;
            }
            else
            {
              document.getElementById("13")!.style.background = this.backgroundColour[Math.floor(Math.random() * 5)][0];
              ++this.numTasks;
              // description
              this.tasks[this.numTasks][0] = this.thirteen;
              // start Time
              this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value+ "T13:00:00Z";
              // end time
              this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T13:15:00Z";
              // Setting the project
              this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1;
              this.projectID = this.activities[this.project][0];
              this.thirteenProject = this.projectID ;
              this.tasks[this.numTasks][3] =  this.projectID ;
              this.thirteenBillable = this.billable;
              this.tasks[this.numTasks][4] = this.thirteenBillable;
            }
          }
        }
      }
    }

  // setting the task for 13:15
    Clicked1315():void{
      // Checking if the project is blank
      if ((document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1 === -1)
      {
        this.ErrorResult = "Project cannot be left blank";
        const x = document.getElementById("ErrorResultMessage")!;
        x.style.display = "block";
      }
      else
      {
        // Duplicating the task setting
        if ((document.getElementById("tasks") as HTMLInputElement).value === "")
        {
          (document.getElementById("tasks") as HTMLInputElement).value = this.thirteenFifteen;
          for (let i = 0; i < this.k ; ++i)
          {
            if (this.activities[i][0] === this.thirteenFifteenProject)
            {
              (document.getElementById("ProjectsList") as HTMLInputElement).value = this.activities[i][1];
            }
          }
          // Setting the billable status
          this.billable = this.thirteenFifteenBillable;
          if (this.billable === "false")
          {
            document.getElementById("billable")!.style.color = "#000000";
          }
          else
          {
            document.getElementById("billable")!.style.color = "#3FA5F7";
          }
        }
        else
        {
          this.thirteenFifteen = (document.getElementById("tasks") as HTMLInputElement).value;
          if (this.thirteenFifteen === this.thirteen)
          {
            this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T13:30:00Z";
            document.getElementById("1315")!.style.background = document.getElementById("13")!.style.backgroundColor;
            this.thirteenFifteenProject = this.thirteenProject;
            this.thirteenFifteenBillable = this.thirteenBillable;
          }
          else
          {
            if (this.thirteenFifteen === this.thirteenThirty)
            {
              document.getElementById("1315")!.style.background = document.getElementById("1330")!.style.backgroundColor;
              this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T13:15:00Z";
              this.thirteenFifteenProject = this.thirteenThirtyProject;
              this.thirteenFifteenBillable = this.thirteenThirtyBillable;
            }
            else
            {
              document.getElementById("1315")!.style.background = this.backgroundColour[Math.floor(Math.random() * 5)][0];
              ++this.numTasks;
              // description
              this.tasks[this.numTasks][0] = this.thirteenFifteen;
              // start Time
              this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T13:15:00Z";
              // end time
              this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T13:30:00Z";
              // Setting the project
              this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1;
              this.projectID = this.activities[this.project][0];
              this.thirteenFifteenProject = this.projectID ;
              this.tasks[this.numTasks][3] =  this.projectID ;
              this.thirteenFifteenBillable = this.billable;
              this.tasks[this.numTasks][4] = this.thirteenFifteenBillable;
            }
          }
        }
      }
    }

  // setting the task for 13:30
    Clicked1330():void{
      // Checking if the project is blank
      if ((document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1 === -1)
      {
        this.ErrorResult = "Project cannot be left blank";
        const x = document.getElementById("ErrorResultMessage")!;
        x.style.display = "block";
      }
      else
      {
        // Duplicating the task setting
        if ((document.getElementById("tasks") as HTMLInputElement).value === "")
        {
          (document.getElementById("tasks") as HTMLInputElement).value = this.thirteenThirty;
          for (let i = 0; i < this.k ; ++i)
          {
            if (this.activities[i][0] === this.thirteenThirtyProject)
            {
              (document.getElementById("ProjectsList") as HTMLInputElement).value = this.activities[i][1];
            }
          }
          // Setting the billable status
          this.billable = this.thirteenThirtyBillable;
          if (this.billable === "false")
          {
            document.getElementById("billable")!.style.color = "#000000";
          }
          else
          {
            document.getElementById("billable")!.style.color = "#3FA5F7";
          }
        }
        else
        {
          this.thirteenThirty = (document.getElementById("tasks") as HTMLInputElement).value;
          if (this.thirteenThirty === this.thirteenFifteen)
          {
            this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T13:45:00Z";
            document.getElementById("1330")!.style.background = document.getElementById("1315")!.style.backgroundColor;
            this.thirteenThirtyProject = this.thirteenFifteenProject;
            this.thirteenThirtyBillable = this.thirteenFifteenProject;
          }
          else
          {
            if (this.thirteenThirty === this.thirteenFortyFive)
            {
              document.getElementById("1330")!.style.background = document.getElementById("1345")!.style.backgroundColor;
              this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T13:30:00Z";
              this.thirteenThirtyProject = this.thirteenFortyFiveProject;
              this.thirteenThirtyBillable = this.thirteenFortyFiveBillable;
            }
            else
            {
              document.getElementById("1330")!.style.background = this.backgroundColour[Math.floor(Math.random() * 5)][0] ;
              ++this.numTasks;
              // description
              this.tasks[this.numTasks][0] = this.thirteenThirty;
              // start Time
              this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T13:30:00Z";
              // end time
              this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T13:45:00Z";
              // Setting the project
              this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1;
              this.projectID = this.activities[this.project][0];
              this.thirteenThirtyProject = this.projectID ;
              this.tasks[this.numTasks][3] =  this.projectID ;
              this.thirteenThirtyBillable = this.billable;
              this.tasks[this.numTasks][4] = this.thirteenThirtyBillable;
            }
          }
        }
      }
    }

  // setting the task for 13:45
    Clicked1345():void{
      // Checking if the project is blank
      if ((document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1 === -1)
      {
        this.ErrorResult = "Project cannot be left blank";
        const x = document.getElementById("ErrorResultMessage")!;
        x.style.display = "block";
      }
      else
      {
        // Duplicating the task setting
        if ((document.getElementById("tasks") as HTMLInputElement).value === "")
        {
          (document.getElementById("tasks") as HTMLInputElement).value = this.thirteenFortyFive;
          for (let i = 0; i < this.k ; ++i)
          {
            if (this.activities[i][0] === this.thirteenFortyFiveProject)
            {
              (document.getElementById("ProjectsList") as HTMLInputElement).value = this.activities[i][1];
            }
          }
          // Setting the billable status
          this.billable = this.thirteenFortyFiveBillable;
          if (this.billable === "false")
          {
            document.getElementById("billable")!.style.color = "#000000";
          }
          else
          {
            document.getElementById("billable")!.style.color = "#3FA5F7";
          }
        }
        else
        {
          this.thirteenFortyFive = (document.getElementById("tasks") as HTMLInputElement).value;
          if (this.thirteenFortyFive === this.thirteenThirty)
          {
            this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T14:00:00Z";
            document.getElementById("1345")!.style.background = document.getElementById("1330")!.style.backgroundColor;
            this.thirteenFortyFiveProject = this.thirteenThirtyProject;
            this.thirteenFortyFiveBillable = this.thirteenThirtyBillable;
          }
          else
          {
            if (this.thirteenFortyFive === this.fourteen)
            {
              document.getElementById("1345")!.style.background = document.getElementById("14")!.style.backgroundColor;
              this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T13:45:00Z";
              this.thirteenFortyFiveProject = this.fourteenProject;
              this.thirteenFortyFiveBillable = this.fourteenBillable;
            }
            else
            {
              document.getElementById("1345")!.style.background = this.backgroundColour[Math.floor(Math.random() * 5)][0] ;
              ++this.numTasks;
              // description
              this.tasks[this.numTasks][0] = this.thirteenFortyFive;
              // start Time
              this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value+ "T13:45:00Z";
              // end time
              this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T14:00:00Z";
              // Setting the project
              this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1;
              this.projectID = this.activities[this.project][0];
              this.thirteenFortyFiveProject = this.projectID ;
              this.tasks[this.numTasks][3] =  this.projectID ;
              this.thirteenFortyFiveBillable = this.billable;
              this.tasks[this.numTasks][4] = this.thirteenFortyFiveBillable;
            }
          }
        }
      }
    }

  // setting the task for 14:00
    Clicked14():void{
      // Checking if the project is blank
      if ((document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1 === -1)
      {
        this.ErrorResult = "Project cannot be left blank";
        const x = document.getElementById("ErrorResultMessage")!;
        x.style.display = "block";
      }
      else
      {
        // Duplicating the task setting
        if ((document.getElementById("tasks") as HTMLInputElement).value === "")
        {
          (document.getElementById("tasks") as HTMLInputElement).value = this.fourteen;
          for (let i = 0; i < this.k ; ++i)
          {
            if (this.activities[i][0] === this.fourteenProject)
            {
              (document.getElementById("ProjectsList") as HTMLInputElement).value = this.activities[i][1];
            }
          }
          // Setting the billable status
          this.billable = this.fourteenBillable;
          if (this.billable === "false")
          {
            document.getElementById("billable")!.style.color = "#000000";
          }
          else
          {
            document.getElementById("billable")!.style.color = "#3FA5F7";
          }
        }
        else
        {
          this.fourteen = (document.getElementById("tasks") as HTMLInputElement).value;
          if (this.fourteen === this.thirteenFortyFive)
          {
            this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T14:15:00Z";
            document.getElementById("14")!.style.background = document.getElementById("1345")!.style.backgroundColor;
            this.fourteenProject =  this.thirteenFortyFiveProject;
            this.fourteenBillable = this.thirteenFortyFiveBillable;
          }
          else
          {
            if (this.fourteen === this.fourteenFifteen)
            {
              document.getElementById("14")!.style.background = document.getElementById("1415")!.style.backgroundColor;
              this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T14:00:00Z";
              this.fourteenProject = this.fourteenFifteenProject;
              this.fourteenBillable = this.fourteenFifteenBillable;
            }
            else
            {
              document.getElementById("14")!.style.background = this.backgroundColour[Math.floor(Math.random() * 5)][0] ;
              ++this.numTasks;
              // description
              this.tasks[this.numTasks][0] = this.fourteen;
              // start Time
              this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T14:00:00Z";
              // end time
              this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T14:15:00Z";
              // Setting the project
              this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1;
              this.projectID = this.activities[this.project][0];
              this.fourteenProject = this.projectID ;
              this.tasks[this.numTasks][3] =  this.projectID ;
              this.fourteenBillable = this.billable;
              this.tasks[this.numTasks][4] = this.fourteenBillable;
            }
          }
        }
      }
    }

  // setting the task for 14:15
    Clicked1415():void{
      // Checking if the project is blank
      if ((document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1 === -1)
      {
        this.ErrorResult = "Project cannot be left blank";
        const x = document.getElementById("ErrorResultMessage")!;
        x.style.display = "block";
      }
      else
      {
        // Duplicating the task setting
        if ((document.getElementById("tasks") as HTMLInputElement).value === "")
        {
          (document.getElementById("tasks") as HTMLInputElement).value = this.fourteenFifteen;
          for (let i = 0; i < this.k ; ++i)
          {
            if (this.activities[i][0] === this.fourteenFifteenProject)
            {
              (document.getElementById("ProjectsList") as HTMLInputElement).value = this.activities[i][1];
            }
          }
          // Setting the billable status
          this.billable = this.fourteenFifteenBillable;
          if (this.billable === "false")
          {
            document.getElementById("billable")!.style.color = "#000000";
          }
          else
          {
            document.getElementById("billable")!.style.color = "#3FA5F7";
          }
        }
        else
        {
          this.fourteenFifteen = (document.getElementById("tasks") as HTMLInputElement).value;
          if (this.fourteenFifteen === this.fourteen)
          {
            this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T14:30:00Z";
            document.getElementById("1415")!.style.background = document.getElementById("14")!.style.backgroundColor;
            this.fourteenFifteenProject = this.fourteenProject;
            this.fourteenFifteenBillable = this.fourteenBillable;
          }
          else
          {
            if (this.fourteenFifteen === this.fourteenThirty)
            {
              document.getElementById("1415")!.style.background = document.getElementById("1430")!.style.backgroundColor;
              this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T14:15:00Z";
              this.fourteenFifteenProject = this.fourteenThirtyProject;
              this.fourteenFifteenBillable = this.fourteenThirtyBillable;
            }
            else
            {
              document.getElementById("1415")!.style.background = this.backgroundColour[Math.floor(Math.random() * 5)][0] ;
              ++this.numTasks;
              // description
              this.tasks[this.numTasks][0] = this.fourteenFifteen;
              // start Time
              this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T14:15:00Z";
              // end time
              this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value+ "T14:30:00Z";
              // Setting the project
              this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1;
              this.projectID = this.activities[this.project][0];
              this.fourteenFifteenProject = this.projectID ;
              this.tasks[this.numTasks][3] =  this.projectID ;
              this.fourteenFifteenBillable = this.billable;
              this.tasks[this.numTasks][4] = this.fourteenFifteenBillable;
            }
          }
        }
      }
    }

    // setting the task for 14:30
    Clicked1430():void{
      // Checking if the project is blank
      if ((document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1 === -1)
      {
        this.ErrorResult = "Project cannot be left blank";
        const x = document.getElementById("ErrorResultMessage")!;
        x.style.display = "block";
      }
      else
      {
        // Duplicating the task setting
        if ((document.getElementById("tasks") as HTMLInputElement).value === "")
        {
          (document.getElementById("tasks") as HTMLInputElement).value = this.fourteenThirty;
          for (let i = 0; i < this.k ; ++i)
          {
            if (this.activities[i][0] === this.fourteenThirtyProject)
            {
              (document.getElementById("ProjectsList") as HTMLInputElement).value = this.activities[i][1];
            }
          }
          // Setting the billable status
          this.billable = this.fourteenThirtyBillable;
          if (this.billable === "false")
          {
            document.getElementById("billable")!.style.color = "#000000";
          }
          else
          {
            document.getElementById("billable")!.style.color = "#3FA5F7";
          }
        }
        else
        {
          this.fourteenThirty = (document.getElementById("tasks") as HTMLInputElement).value;
          if (this.fourteenThirty === this.fourteenFifteen)
          {
            this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value+ "T14:45:00Z";
            document.getElementById("1430")!.style.background = document.getElementById("1415")!.style.backgroundColor;
            this.fourteenThirtyProject = this.fourteenFifteenProject;
            this.fourteenThirtyBillable = this.fourteenFifteenBillable;
          }
          else
          {
            if (this.fourteenThirty === this.fourteenFortyFive)
            {
              document.getElementById("1430")!.style.background = document.getElementById("1445")!.style.backgroundColor;
              this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T14:30:00Z";
              this.fourteenThirtyProject = this.fourteenFortyFiveProject;
              this.fourteenThirtyBillable = this.fourteenFortyFiveBillable;
            }
            else
            {
              document.getElementById("1430")!.style.background = this.backgroundColour[Math.floor(Math.random() * 5)][0] ;
              ++this.numTasks;
              // description
              this.tasks[this.numTasks][0] = this.fourteenThirty;
              // start Time
              this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T14:30:00Z";
              // end time
              this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T14:45:00Z";
              // Setting the project
              this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1;
              this.projectID = this.activities[this.project][0];
              this.fourteenThirtyProject = this.projectID ;
              this.tasks[this.numTasks][3] =  this.projectID ;
              this.fourteenThirtyBillable = this.billable;
              this.tasks[this.numTasks][4] = this.fourteenThirtyBillable;
            }
          }
        }
      }
    }

    // setting the task for 14:45
    Clicked1445():void{
      // Checking if the project is blank
      if ((document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1 === -1)
      {
        this.ErrorResult = "Project cannot be left blank";
        const x = document.getElementById("ErrorResultMessage")!;
        x.style.display = "block";
      }
      else
      {
        // Duplicating the task setting
        if ((document.getElementById("tasks") as HTMLInputElement).value === "")
        {
          (document.getElementById("tasks") as HTMLInputElement).value = this.fourteenFortyFive;
          for (let i = 0; i < this.k ; ++i)
          {
            if (this.activities[i][0] === this.fourteenFortyFiveProject)
            {
              (document.getElementById("ProjectsList") as HTMLInputElement).value = this.activities[i][1];
            }
          }
          // Setting the billable status
          this.billable = this.fourteenFortyFiveBillable;
          if (this.billable === "false")
          {
            document.getElementById("billable")!.style.color = "#000000";
          }
          else
          {
            document.getElementById("billable")!.style.color = "#3FA5F7";
          }
        }
        else
        {
          this.fourteenFortyFive = (document.getElementById("tasks") as HTMLInputElement).value;
          if (this.fourteenFortyFive === this.fourteenThirty)
          {
            this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T15:00:00Z";
            document.getElementById("1445")!.style.background = document.getElementById("1430")!.style.backgroundColor;
            this.fourteenFortyFiveProject = this.fourteenThirtyProject;
            this.fourteenFortyFiveBillable = this.fourteenThirtyBillable;
          }
          else
          {
            if (this.fourteenFortyFive === this.fifteen)
            {
              document.getElementById("1445")!.style.background = document.getElementById("15")!.style.backgroundColor;
              this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value+ "T14:45:00Z";
              this.fourteenFortyFiveProject = this.fifteenProject;
              this.fourteenFortyFiveBillable = this.fifteenBillable;
            }
            else
            {
              document.getElementById("1445")!.style.background = this.backgroundColour[Math.floor(Math.random() * 5)][0] ;
              ++this.numTasks;
              // description
              this.tasks[this.numTasks][0] = this.fourteenFortyFive;
              // start Time
              this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T14:45:00Z";
              // end time
              this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T15:00:00Z";
              // Setting the project
              this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1;
              this.projectID = this.activities[this.project][0];
              this.fourteenFortyFiveProject = this.projectID ;
              this.tasks[this.numTasks][3] =  this.projectID ;
              this.fourteenFortyFiveBillable = this.billable;
              this.tasks[this.numTasks][4] = this.fourteenFortyFiveBillable;
            }
          }
        }
      }
    }

    // setting the task for 15:00
    Clicked15():void{
      // Checking if the project is blank
      if ((document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1 === -1)
      {
        this.ErrorResult = "Project cannot be left blank";
        const x = document.getElementById("ErrorResultMessage")!;
        x.style.display = "block";
      }
      else
      {
        // Duplicating the task setting
        if ((document.getElementById("tasks") as HTMLInputElement).value === "")
        {
          (document.getElementById("tasks") as HTMLInputElement).value = this.fifteen;
          for (let i = 0; i < this.k ; ++i)
          {
            if (this.activities[i][0] === this.fifteenProject)
            {
              (document.getElementById("ProjectsList") as HTMLInputElement).value = this.activities[i][1];
            }
          }
          // Setting the billable status
          this.billable = this.fifteenBillable;
          if (this.billable === "false")
          {
            document.getElementById("billable")!.style.color = "#000000";
          }
          else
          {
            document.getElementById("billable")!.style.color = "#3FA5F7";
          }
        }
        else
        {
          this.fifteen = (document.getElementById("tasks") as HTMLInputElement).value;
          if (this.fifteen === this.fourteenFortyFive)
          {
            this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T15:15:00Z";
            document.getElementById("15")!.style.background = document.getElementById("1445")!.style.backgroundColor;
            this.fifteenProject = this.fourteenFortyFiveProject;
            this.fifteenBillable = this.fourteenFortyFiveBillable;
          }
          else
          {
            if (this.fifteen === this.fifteenFifteen)
            {
              document.getElementById("15")!.style.background = document.getElementById("1515")!.style.backgroundColor;
              this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T15:00:00Z";
              this.fifteenProject = this.fifteenFifteenProject;
              this.fifteenBillable = this.fifteenFifteenBillable;
            }
            else
            {
              document.getElementById("15")!.style.background = this.backgroundColour[Math.floor(Math.random() * 5)][0] ;
              ++this.numTasks;
              // description
              this.tasks[this.numTasks][0] = this.fifteen;
              // start Time
              this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T15:00:00Z";
              // end time
              this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T15:15:00Z";
              // Setting the project
              this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1;
              this.projectID = this.activities[this.project][0];
              this.fifteenProject = this.projectID ;
              this.tasks[this.numTasks][3] =  this.projectID ;
              this.fifteenBillable = this.billable;
              this.tasks[this.numTasks][4] = this.fifteenBillable;
            }
          }
        }
      }
    }

  // setting the task for 15:15
    Clicked1515():void{
      // Checking if the project is blank
      if ((document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1 === -1)
      {
        this.ErrorResult = "Project cannot be left blank";
        const x = document.getElementById("ErrorResultMessage")!;
        x.style.display = "block";
      }
      else
      {
        // Duplicating the task setting
        if ((document.getElementById("tasks") as HTMLInputElement).value === "")
        {
          (document.getElementById("tasks") as HTMLInputElement).value = this.fifteenFifteen;
          for (let i = 0; i < this.k ; ++i)
          {
            if (this.activities[i][0] === this.fifteenFifteenProject)
            {
              (document.getElementById("ProjectsList") as HTMLInputElement).value = this.activities[i][1];
            }
          }
          // Setting the billable status
          this.billable = this.fifteenFifteenBillable;
          if (this.billable === "false")
          {
            document.getElementById("billable")!.style.color = "#000000";
          }
          else
          {
            document.getElementById("billable")!.style.color = "#3FA5F7";
          }
        }
        else
        {
          this.fifteenFifteen = (document.getElementById("tasks") as HTMLInputElement).value;
          if (this.fifteenFifteen === this.fifteen)
          {
            this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T15:30:00Z";
            document.getElementById("1515")!.style.background = document.getElementById("15")!.style.backgroundColor;
            this.fifteenFifteenProject = this.fifteenProject;
            this.fifteenFifteenBillable = this.fifteenBillable;
          }
          else
          {
            if (this.fifteenFifteen === this.fifteenThirty)
            {
              document.getElementById("1515")!.style.background = document.getElementById("1530")!.style.backgroundColor;
              this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T15:15:00Z";
              this.fifteenFifteenProject = this.fifteenThirtyProject;
              this.fifteenFifteenBillable = this.fifteenThirtyBillable;
            }
            else
            {
              document.getElementById("1515")!.style.background = this.backgroundColour[Math.floor(Math.random() * 5)][0];
              ++this.numTasks;
              // description
              this.tasks[this.numTasks][0] = this.fifteenFifteen;
              // start Time
              this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T15:15:00Z";
              // end time
              this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T15:30:00Z";
              // Setting the project
              this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1;
              this.projectID = this.activities[this.project][0];
              this.fifteenFifteenProject = this.projectID ;
              this.tasks[this.numTasks][3] =  this.projectID ;
              this.fifteenFifteenBillable = this.billable;
              this.tasks[this.numTasks][4] = this.fifteenFifteenBillable;
            }
          }
        }
      }
    }

    // setting the task for 15:30
    Clicked1530():void{
      // Checking if the project is blank
      if ((document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1 === -1)
      {
        this.ErrorResult = "Project cannot be left blank";
        const x = document.getElementById("ErrorResultMessage")!;
        x.style.display = "block";
      }
      else
      {
        // Duplicating the task setting
        if ((document.getElementById("tasks") as HTMLInputElement).value === "")
        {
          (document.getElementById("tasks")as HTMLInputElement).value = this.fifteenThirty;
          for (let i = 0; i < this.k ; ++i)
          {
            if (this.activities[i][0] === this.fifteenThirtyProject)
            {
              (document.getElementById("ProjectsList") as HTMLInputElement).value = this.activities[i][1];
            }
          }
          // Setting the billable status
          this.billable = this.fifteenThirtyBillable;
          if (this.billable === "false")
          {
            document.getElementById("billable")!.style.color = "#000000";
          }
          else
          {
            document.getElementById("billable")!.style.color = "#3FA5F7";
          }
        }
        else
        {
          this.fifteenThirty = (document.getElementById("tasks") as HTMLInputElement).value;
          if (this.fifteenThirty === this.fifteenFifteen)
          {
            this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T15:45:00Z";
            document.getElementById("1530")!.style.background = document.getElementById("1515")!.style.backgroundColor;
            this.fifteenThirtyProject = this.fifteenFifteenProject;
            this.fifteenThirtyBillable = this.fifteenFifteenBillable;
          }
          else
          {
            if (this.fifteenThirty === this.fifteenFortyFive)
            {
              document.getElementById("1530")!.style.background = document.getElementById("1545")!.style.backgroundColor;
              this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T15:30:00Z";
              this.fifteenThirtyProject = this.fifteenFortyFiveProject;
              this.fifteenThirtyBillable = this.fifteenFortyFiveBillable;
            }
            else
            {
              document.getElementById("1530")!.style.background = this.backgroundColour[Math.floor(Math.random() * 5)][0] ;
              ++this.numTasks;
              // description
              this.tasks[this.numTasks][0] = this.fifteenThirty;
              // start Time
              this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T15:30:00Z";
              // end time
              this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T15:45:00Z";
              // Setting the project
              this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1;
              this.projectID = this.activities[this.project][0];
              this.fifteenThirtyProject = this.projectID ;
              this.tasks[this.numTasks][3] =  this.projectID ;
              this.fifteenThirtyBillable = this.billable;
              this.tasks[this.numTasks][4] = this.fifteenThirtyBillable;
            }
          }
        }
      }
    }

    // setting the task for 15:45
    Clicked1545():void{
      // Checking if the project is blank
      if ((document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1 === -1)
      {
        this.ErrorResult = "Project cannot be left blank";
        const x = document.getElementById("ErrorResultMessage")!;
        x.style.display = "block";
      }
      else
      {
        // Duplicating the task setting
        if ((document.getElementById("tasks") as HTMLInputElement).value === "")
        {
          (document.getElementById("tasks") as HTMLInputElement).value = this.fifteenFortyFive;
          for (let i = 0; i < this.k ; ++i)
          {
            if (this.activities[i][0] === this.fifteenFortyFiveProject)
            {
              (document.getElementById("ProjectsList") as HTMLInputElement).value = this.activities[i][1];
            }
          }
          // Setting the billable status
          this.billable = this.fifteenFortyFiveBillable;
          if (this.billable === "false")
          {
            document.getElementById("billable")!.style.color = "#000000";
          }
          else
          {
            document.getElementById("billable")!.style.color = "#3FA5F7";
          }
        }
        else
        {
          this.fifteenFortyFive = (document.getElementById("tasks") as HTMLInputElement).value;
          if (this.fifteenFortyFive === this.fifteenThirty)
          {
            this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T16:00:00Z";
            document.getElementById("1545")!.style.background = document.getElementById("1530")!.style.backgroundColor;
            this.fifteenFortyFiveProject = this.fifteenThirtyProject;
            this.fifteenFortyFiveBillable = this.fifteenThirtyBillable;
          }
          else
          {
            if (this.fifteenFortyFive === this.sixteen)
            {
              document.getElementById("1545")!.style.background = document.getElementById("16")!.style.backgroundColor;
              this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T15:45:00Z";
              this.fifteenFortyFiveProject = this.sixteenProject;
              this.fifteenFortyFiveBillable = this.sixteenBillable;
            }
            else
            {
              document.getElementById("1545")!.style.background = this.backgroundColour[Math.floor(Math.random() * 5)  ][0] ;
              ++this.numTasks;
              // description
              this.tasks[this.numTasks][0] = this.fifteenFortyFive;
              // start Time
              this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T15:45:00Z";
              // end time
              this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T16:00:00Z";
              // Setting the project
              this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1;
              this.projectID = this.activities[this.project][0];
              this.fifteenFortyFiveProject = this.projectID ;
              this.tasks[this.numTasks][3] =  this.projectID ;
              this.fifteenFortyFiveBillable = this.billable;
              this.tasks[this.numTasks][4] = this.fifteenFortyFiveBillable;
            }
          }
        }
      }
    }


    // setting the task for 16:00
    Clicked16():void{
      // Checking if the project is blank
      if ((document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1 === -1)
      {
        this.ErrorResult = "Project cannot be left blank";
        const x = document.getElementById("ErrorResultMessage")!;
        x.style.display = "block";
      }
      else
      {
        // Duplicating the task setting
        if ((document.getElementById("tasks") as HTMLInputElement).value === "")
        {
          (document.getElementById("tasks") as HTMLInputElement).value = this.sixteen;
          for (let i = 0; i < this.k ; ++i)
          {
            if (this.activities[i][0] === this.sixteenProject)
            {
              (document.getElementById("ProjectsList") as HTMLInputElement).value = this.activities[i][1];
            }
          }
          // Setting the billable status
          this.billable = this.sixteenBillable;
          if (this.billable === "false")
          {
            document.getElementById("billable")!.style.color = "#000000";
          }
          else
          {
            document.getElementById("billable")!.style.color = "#3FA5F7";
          }
        }
        else
        {
          this.sixteen = (document.getElementById("tasks") as HTMLInputElement).value;
          if (this.sixteen === this.fifteenFortyFive)
          {
            this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T16:15:00Z";
            document.getElementById("16")!.style.background = document.getElementById("1545")!.style.backgroundColor;
            this.sixteenProject  = this.fifteenFortyFiveProject;
            this.sixteenBillable = this.fifteenFortyFiveBillable;
          }
          else
          {
            if (this.sixteen === this.sixteenFifteen)
            {
              document.getElementById("16")!.style.background = document.getElementById("1615")!.style.backgroundColor;
              this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T16:00:00Z";
              this.sixteenProject = this.sixteenFifteenProject;
              this.sixteenBillable = this.sixteenFifteenBillable;
            }
            else
            {
              document.getElementById("16")!.style.background = this.backgroundColour[Math.floor(Math.random() * 5)  ][0] ;
              ++this.numTasks;
              // description
              this.tasks[this.numTasks][0] = this.sixteen;
              // start Time
              this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value+ "T16:00:00Z";
              // end time
              this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T16:15:00Z";
              // Setting the project
              this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1;
              this.projectID = this.activities[this.project][0];
              this.sixteenProject = this.projectID ;
              this.tasks[this.numTasks][3] =  this.projectID ;
              this.sixteenBillable = this.billable;
              this.tasks[this.numTasks][4] = this.sixteenBillable;
            }
          }
        }
      }
    }


    // setting the task for 16:15
    Clicked1615():void{
      // Checking if the project is blank
      if ((document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1 === -1)
      {
        this.ErrorResult = "Project cannot be left blank";
        const x = document.getElementById("ErrorResultMessage")!;
        x.style.display = "block";
      }
      else
      {
        // Duplicating the task setting
        if ((document.getElementById("tasks") as HTMLInputElement).value === "")
        {
          (document.getElementById("tasks") as HTMLInputElement).value = this.sixteenFifteen;
          for (let i = 0; i < this.k ; ++i)
          {
            if (this.activities[i][0] === this.sixteenFifteenProject)
            {
              (document.getElementById("ProjectsList") as HTMLInputElement).value = this.activities[i][1];
            }
          }
          // Setting the billable status
          this.billable = this.sixteenFifteenBillable;
          if (this.billable === "false")
          {
            document.getElementById("billable")!.style.color = "#000000";
          }
          else
          {
            document.getElementById("billable")!.style.color = "#3FA5F7";
          }
        }
        else
        {
          this.sixteenFifteen = (document.getElementById("tasks") as HTMLInputElement).value;
          if (this.sixteenFifteen === this.sixteen)
          {
            this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T16:30:00Z";
            document.getElementById("1615")!.style.background = document.getElementById("16")!.style.backgroundColor;
            this.sixteenFifteenProject = this.sixteenProject;
            this.sixteenFifteenBillable = this.sixteenBillable;
          }
          else
          {
            if (this.sixteenFifteen === this.sixteenThirty)
            {
              document.getElementById("1615")!.style.background = document.getElementById("1630")!.style.backgroundColor;
              this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value+ "T16:15:00Z";
              this.sixteenFifteenProject  = this.sixteenThirtyProject;
              this.sixteenFifteenBillable = this.sixteenThirtyBillable;
            }
            else
            {
              document.getElementById("1615")!.style.background = this.backgroundColour[Math.floor(Math.random() * 5)  ][0] ;
              ++this.numTasks;
              // description
              this.tasks[this.numTasks][0] = this.sixteenFifteen;
              // start Time
              this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T16:15:00Z";
              // end time
              this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T16:30:00Z";
              // Setting the project
              this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1;
              this.projectID = this.activities[this.project][0];
              this.sixteenFifteenProject = this.projectID ;
              this.tasks[this.numTasks][3] =  this.projectID ;
              this.sixteenFifteenBillable = this.billable;
              this.tasks[this.numTasks][4] = this.sixteenFifteenBillable;
            }
          }
        }
      }
    }


    // setting the task for 16:30
    Clicked1630():void{
      // Checking if the project is blank
      if ((document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1 === -1)
      {
        this.ErrorResult = "Project cannot be left blank";
        const x = document.getElementById("ErrorResultMessage")!;
        x.style.display = "block";
      }
      else
      {
        // Duplicating the task setting
        if ((document.getElementById("tasks") as HTMLInputElement).value === "")
        {
          (document.getElementById("tasks") as HTMLInputElement).value = this.sixteenThirty;
          for (let i = 0; i < this.k ; ++i)
          {
            if (this.activities[i][0] === this.sixteenThirtyProject)
            {
              (document.getElementById("ProjectsList") as HTMLInputElement).value = this.activities[i][1];
            }
          }
          // Setting the billable status
          this.billable = this.sixteenThirtyBillable;
          if (this.billable === "false")
          {
            document.getElementById("billable")!.style.color = "#000000";
          }
          else
          {
            document.getElementById("billable")!.style.color = "#3FA5F7";
          }
        }
        else
        {
          this.sixteenThirty = (document.getElementById("tasks") as HTMLInputElement).value;
          if (this.sixteenThirty === this.sixteenFifteen)
          {
            this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T16:45:00Z";
            document.getElementById("1630")!.style.background = document.getElementById("1615")!.style.backgroundColor;
            this.sixteenThirtyProject = this.sixteenFifteenProject;
            this.sixteenThirtyBillable = this.sixteenFifteenBillable;
          }
          else
          {
            if (this.sixteenThirty === this.sixteenForthFive)
            {
              document.getElementById("1630")!.style.background = document.getElementById("1645")!.style.backgroundColor;
              this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T16:30:00Z";
              this.sixteenThirtyProject = this.sixteenForthFiveProject;
              this.sixteenThirtyBillable = this.sixteenForthFiveBillable;
            }
            else
            {
              document.getElementById("1630")!.style.background = this.backgroundColour[Math.floor(Math.random() * 5)  ][0] ;
              ++this.numTasks;
              // description
              this.tasks[this.numTasks][0] = this.sixteenThirty;
              // start Time
              this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T16:30:00Z";
              // end time
              this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T16:45:00Z";
              // Setting the project
              this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1;
              this.projectID = this.activities[this.project][0];
              this.sixteenThirtyProject = this.projectID ;
              this.tasks[this.numTasks][3] =  this.projectID ;
              this.sixteenThirtyBillable = this.billable;
              this.tasks[this.numTasks][4] = this.sixteenThirtyBillable;
            }
          }
        }
      }
    }


    // setting the task for 16:45
    Clicked1645():void{
      // Checking if the project is blank
      if ((document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1 === -1)
      {
        this.ErrorResult = "Project cannot be left blank";
        const x = document.getElementById("ErrorResultMessage")!;
        x.style.display = "block";
      }
      else
      {
        // Duplicating the task setting
        if ((document.getElementById("tasks") as HTMLInputElement).value === "")
        {
          (document.getElementById("tasks") as HTMLInputElement).value = this.sixteenForthFive;
          for (let i = 0; i < this.k ; ++i)
          {
            if (this.activities[i][0] === this.sixteenForthFiveProject)
            {
              (document.getElementById("ProjectsList") as HTMLInputElement).value = this.activities[i][1];
            }
          }
          // Setting the billable status
          this.billable = this.sixteenForthFiveBillable;
          if (this.billable === "false")
          {
            document.getElementById("billable")!.style.color = "#000000";
          }
          else
          {
            document.getElementById("billable")!.style.color = "#3FA5F7";
          }
        }
        else
        {
          this.sixteenForthFive = (document.getElementById("tasks") as HTMLInputElement).value;
          if (this.sixteenForthFive === this.sixteenThirty)
          {
            this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked") as HTMLInputElement).value + "T17:00:00Z";
            document.getElementById("1645")!.style.background = document.getElementById("1630")!.style.backgroundColor;
            this.sixteenForthFiveProject = this.sixteenThirtyProject;
            this.sixteenForthFiveBillable = this.sixteenThirtyBillable;
          }
          else
          {
            if (this.sixteenForthFive === this.seventeen)
            {
              document.getElementById("1645")!.style.background = document.getElementById("17")!.style.backgroundColor;
              this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T16:45:00Z";
              this.sixteenForthFiveProject = this.seventeenProject;
              this.sixteenForthFiveBillable = this.seventeenBillable;
            }
            else
            {
              document.getElementById("1645")!.style.background = this.backgroundColour[Math.floor(Math.random() * 5)  ][0] ;
              ++this.numTasks;
              // description
              this.tasks[this.numTasks][0] = this.sixteenForthFive;
              // start Time
              this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T16:45:00Z";
              // end time
              this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T17:00:00Z";
              // Setting the project
              this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1;
              this.projectID = this.activities[this.project][0];
              this.sixteenForthFiveProject = this.projectID ;
              this.tasks[this.numTasks][3] =  this.projectID ;
              this.sixteenForthFiveBillable = this.billable;
              this.tasks[this.numTasks][4] = this.sixteenForthFiveBillable;
            }
          }
        }
      }
    }

    // setting the task for 17:00
    Clicked17():void{
      // Checking if the project is blank
      if ((document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1 === -1)
      {
        this.ErrorResult = "Project cannot be left blank";
        const x = document.getElementById("ErrorResultMessage")!;
        x.style.display = "block";
      }
      else
      {
        // Duplicating the task setting
        if ((document.getElementById("tasks") as HTMLInputElement).value === "")
        {
          (document.getElementById("tasks") as HTMLInputElement).value = this.seventeen;
          for (let i = 0; i < this.k ; ++i)
          {
            if (this.activities[i][0] === this.seventeenProject)
            {
              (document.getElementById("ProjectsList") as HTMLInputElement).value = this.activities[i][1];
            }
          }
          // Setting the billable status
          this.billable = this.seventeenBillable;
          if (this.billable === "false")
          {
            document.getElementById("billable")!.style.color = "#000000";
          }
          else
          {
            document.getElementById("billable")!.style.color = "#3FA5F7";
          }
        }
        else
        {
          this.seventeen = (document.getElementById("tasks") as HTMLInputElement).value;
          if (this.seventeen === this.sixteenForthFive)
          {
            this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T17:15:00Z";
            document.getElementById("17")!.style.background = document.getElementById("1645")!.style.backgroundColor;
            this.seventeenProject = this.sixteenForthFiveProject;
            this.seventeenBillable = this.sixteenForthFiveBillable;
          }
          else
          {
            if (this.seventeen === this.seventeenFifteen)
            {
              document.getElementById("17")!.style.background = document.getElementById("1715")!.style.backgroundColor;
              this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T17:00:00Z";
              this.seventeenProject = this.seventeenFifteenProject;
              this.seventeenBillable = this.seventeenFifteenBillable;
            }
            else
            {
              document.getElementById("17")!.style.background = this.backgroundColour[Math.floor(Math.random() * 5)  ][0] ;
              ++this.numTasks;
              // description
              this.tasks[this.numTasks][0] = this.seventeen;
              // start Time
              this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value+ "T17:00:00Z";
              // end time
              this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T17:15:00Z";
              // Setting the project
              this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1;
              this.projectID = this.activities[this.project][0];
              this.seventeenProject = this.projectID ;
              this.tasks[this.numTasks][3] =  this.projectID ;
              this.seventeenBillable = this.billable;
              this.tasks[this.numTasks][4] = this.seventeenBillable;
            }
          }
        }
      }
    }

    // setting the task for 17:15
    Clicked1715():void{
      // Checking if the project is blank
      if ((document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1 === -1)
      {
        this.ErrorResult = "Project cannot be left blank";
        const x = document.getElementById("ErrorResultMessage")!;
        x.style.display = "block";
      }
      else
      {
        // Duplicating the task setting
        if ((document.getElementById("tasks") as HTMLInputElement).value === "")
        {
          (document.getElementById("tasks") as HTMLInputElement).value = this.seventeenFifteen;
          for (let i = 0; i < this.k ; ++i)
          {
            if (this.activities[i][0] === this.seventeenFifteenProject)
            {
              (document.getElementById("ProjectsList") as HTMLInputElement).value = this.activities[i][1];
            }
          }
          // Setting the billable status
          this.billable = this.seventeenFifteenBillable;
          if (this.billable === "false")
          {
            document.getElementById("billable")!.style.color = "#000000";
          }
          else
          {
            document.getElementById("billable")!.style.color = "#3FA5F7";
          }
        }
        else
        {
         this.seventeenFifteen = (document.getElementById("tasks") as HTMLInputElement).value;
          if (this.seventeenFifteen === this.seventeen)
          {
           this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T17:30:00Z";
           document.getElementById("1715")!.style.background = document.getElementById("17")!.style.backgroundColor;
           this.seventeenFifteenProject = this.seventeenProject;
           this.seventeenFifteenBillable = this.seventeenBillable;
          }
          else
          {
            document.getElementById("1715")!.style.background = this.backgroundColour[Math.floor(Math.random() * 5)  ][0];
            ++this.numTasks;
            // description
            this.tasks[this.numTasks][0] = this.seventeenFifteen;
            // start Time
            this.tasks[this.numTasks][1] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T17:15:00Z";
            // end time
            this.tasks[this.numTasks][2] =  (document.getElementById("DatePicked")as HTMLInputElement).value + "T17:30:00Z";
            // Setting the project
            this.project = (document.getElementById("ProjectsList") as HTMLSelectElement)!.selectedIndex-1;
            this.projectID = this.activities[this.project][0];
            this.seventeenFifteenProject = this.projectID ;
            this.tasks[this.numTasks][3] =  this.projectID ;
            this.seventeenFifteenBillable = this.billable;
            this.tasks[this.numTasks][4] = this.seventeenFifteenBillable;
          }
        }
      }
    }


  // Clearing the input for new tasks
  // When the button next task is clicked
  ClearInput():void{
    (document.getElementById("tasks") as HTMLInputElement).value = "";
    const billable = document.getElementById("billable")!;
    billable.style.color = "#000000";
    this.billable = "false";
  }



  // When add to Clockify Clicked
  onClick():void
    {
      this.accountService.getProjects().subscribe(activities => this.activities = activities.array);
       // adding to the project drop down menu
              const select = document.getElementById("ProjectsList")!;

              // Checking how many projects will be added to the drop down
              if (this.k === 0){
              while (this.activities[this.k][0] != null)
              {
                  ++this.k;
              }
              // Adding to the drop down
              for (let i = 0; i < this.k; i++)
              {
                const optn = this.activities[i][1];
                const el = document.createElement("option")!;
                el.textContent = optn;
                el.value = optn;
                select.appendChild(el);
              }
              }
     /* const x = document.getElementById("myDiv");
      if (x.style.display === "none")
      {
        this.accountService.getProjects().subscribe(activities => this.activities = activities.array);
        x.style.display = "block";

        // adding to the project drop down menu
        const select = document.getElementById("ProjectsList");

        // Checking how many projects will be added to the drop down
        if (this.k === 0){
        while (this.activities[this.k][0] != null)
        {
            ++this.k;
        }
        // Adding to the drop down
        for (let i = 0; i < this.k; i++)
        {
          const optn = this.activities[i][1];
          const el = document.createElement("option");
          el.textContent = optn;
          el.value = optn;
          select.appendChild(el);
        }
        }
      }
      else {
        x.style.display = "none";
      } */
    }


   // onProjectClick():void{
     //  this.project = document.getElementById("ProjectsList").selectedIndex;
     // }

  // Adding the time entries to Clockify
  Add():void{

    for (let k = 0 ; k <= this.numTasks ; ++k)
    {
      // Entering and saving the data in TimeEntry.ts
      this.timeEntry =  { description: this.tasks[k][0] , projectId : this.tasks[k][3] , start : this.tasks[k][1]  , end : this.tasks[k][2]  , billableStatus : this.tasks[k][4] };

      // Calling the method in account.service.ts
      this.accountService.posting(this.timeEntry).subscribe();
      const x = document.getElementById("Message")!;
      x.style.display = "block";
    }
  }


  ngOnInit(): void {
    this.refresh();
  }

  refresh(): void {
    this.updatingMetrics = true;
    this.metricsService
      .getMetrics()
      .pipe(
        flatMap(
          () => this.metricsService.threadDump(),
          (metrics: Metrics, threadDump: ThreadDump) => {
            this.metrics = metrics;
            this.threads = threadDump.threads;
            this.updatingMetrics = false;
            this.changeDetector.detectChanges();
          }
        )
      )
      .subscribe();
  }

  metricsKeyExists(key: MetricsKey): boolean {
    return this.metrics && this.metrics[key];
  }

  metricsKeyExistsAndObjectNotEmpty(key: MetricsKey): boolean {
    return this.metrics && this.metrics[key] && JSON.stringify(this.metrics[key]) !== '{}';
  }
}
