import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { TimesheetAssistantSharedModule } from 'app/shared/shared.module';
import { TimesheetAssistantCoreModule } from 'app/core/core.module';
import { TimesheetAssistantAppRoutingModule } from './app-routing.module';
import { TimesheetAssistantHomeModule } from './home/home.module';
import { TimesheetAssistantEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ErrorComponent } from './layouts/error/error.component';



@NgModule({
  imports: [
    BrowserModule,
    TimesheetAssistantSharedModule,
    TimesheetAssistantCoreModule,
    TimesheetAssistantHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    TimesheetAssistantEntityModule,
    TimesheetAssistantAppRoutingModule,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, FooterComponent],
  bootstrap: [MainComponent],
})
export class TimesheetAssistantAppModule {}
