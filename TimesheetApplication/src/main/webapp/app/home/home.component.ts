import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { LoginModalService } from 'app/core/login/login-modal.service';
import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/user/account.model';

// This service provides convenient methods for generating controls.
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'jhi-home',
  templateUrl: './home.component.html',
  styleUrls: ['home.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {
  account: Account | null = null;
  authSubscription?: Subscription;
  data = "hello world";
  result = "";
  workspace = "";
  TextBoxPlaceHolder = "Enter Api key";
  Api = "";
  ShowOrHide = "Show";

  checkoutForm = this.formBuilder.group({
        name: ''
      });


    userInfo : UserInfo = null;

  constructor(private accountService: AccountService, private loginModalService: LoginModalService,private formBuilder: FormBuilder) {this.userInfo = {}}

  onSubmit(): void {
        this.userInfo =  { apiKey: this.checkoutForm.value.name }
        this.Api = this.checkoutForm.value.name;
        localStorage.setItem("setApi",this.Api);
        // Process checkout data here
        this.accountService.getUserInfo(this.userInfo).subscribe(result => this.result = result.login);
        this.accountService.getUserInfo(this.userInfo).subscribe(result => this.result = result.login);
        localStorage.setItem("setUserID",this.result);
        this.checkoutForm.reset();

        this.accountService.getUserWorkspaceID().subscribe(workspace => this.workspace = workspace.login);

        if (this.result === "error in getting user ID")
        {
           const x = document.getElementById("invalidMessage")!;
            x.style.display = "block";
            const labelSignIn = "Sign In";
            localStorage.setItem("SignInLabel",labelSignIn);
        }
        else
        {
          const y = document.getElementById("invalidMessage")!;
          y.style.display = "none";
        //  const x = document.getElementById("Message")!;
        //  x.style.display = "block";
          const labelSignIn = "Account Information";
          localStorage.setItem("SignInLabel",labelSignIn);
          document.getElementById("SignInPage")!.style.display = "none";
          this.Api = "***************";
          document.getElementById("SignOutPage")!.style.display = "block";

        }

      }

      OkClicked():void{
          const x = document.getElementById("Message")!;
          x.style.display = "none";

        }

  SignIn() : void{
      console.log("Sign in clicked");
  }

  SignOut():void
  {
    const labelSignIn = "Sign In";
    localStorage.setItem("SignInLabel",labelSignIn);
    document.getElementById("SignInPage")!.style.display = "block";
    document.getElementById("SignOutPage")!.style.display = "none";
  }

  ShowAndHide():void{
    if (this.ShowOrHide === "Show")
    {
      this.Api = localStorage.getItem("setApi");
      document.getElementById("visible")!.style.color = "#3FA5F7";
      this.ShowOrHide = "Hide";
      document.getElementById("visible")!.style.display = "block";
    }
    else
    {
      document.getElementById("visible")!.style.color = "#000000";
      this.ShowOrHide = "Show";
      this.Api = "***************";
      document.getElementById("visible")!.style.display = "block";
    }
   // (document.getElementById("ShowAndHide") as HtmlInputElement)!.value = "Hide";
  }

  GotoClokifyAccount():void{
    window.location.href = "https://clockify.me/tracker";
  }

  ngOnInit(): void {
    if (localStorage.getItem("SignInLabel") === "Account Information")
    {
      this.userInfo =  { apiKey: localStorage.getItem("setApi")}
      this.accountService.getUserInfo(this.userInfo).subscribe(result => this.result = result.login);
      this.accountService.getUserWorkspaceID().subscribe(workspace => this.workspace = workspace.login);
      this.Api = "***************";
      document.getElementById("SignInPage")!.style.display = "none";
      document.getElementById("SignOutPage")!.style.display = "block";
      console.log("result: " + localStorage.getItem("setUserID"));


    }
    else if (localStorage.getItem("SignInLabel") === "Sign In")
    {
      document.getElementById("SignInPage")!.style.display = "block";
      document.getElementById("SignOutPage")!.style.display = "none";
      this.loginModalService.open();
      this.authSubscription = this.accountService.getAuthenticationState().subscribe(account => (this.account = account));
    }
  }

  isAuthenticated(): boolean {
    return this.accountService.isAuthenticated();
  }

  login(): void {
    this.loginModalService.open();
  }

  ngOnDestroy(): void {
    if (this.authSubscription) {

      this.authSubscription.unsubscribe();
    }
  }
}
